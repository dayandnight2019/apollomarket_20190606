<?php include('header.php');
	if(!isset($_SESSION['id'])) { header('Location: ../members/login.php'); }
	if(isset($_REQUEST['user']) and $_REQUEST['user']!='' ){
		$username = htmlspecialchars($_REQUEST['user']);
		$ConversationPartner_User_Info = $bdd->prepare("SELECT * FROM members WHERE username = ?");
		$ConversationPartner_User_Info->execute(array($username));
		$ConversationPartner_User_Info = $ConversationPartner_User_Info->fetch();
	}
?>


<div class="container">
<h3 class=" text-center">Chat Seller/Customer</h3>
<div class="messaging">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Recent</h4>
            </div>
          </div>
          <div class="inbox_chat">
            <div class="allusers chat_list">

            </div>
          </div>
        </div>
        <div class="mesgs">
          <div class="message msg_history">

          </div>
          <div class="type_msg">
            <div class="input_msg_write">
            <?php 	if( isset($_REQUEST['user']) and $_REQUEST['user']!='' ) { ?>
            <form id="MessageFrm_<?=$ConversationPartner_User_Info['id']?>">
              <input name="message_<?=$ConversationPartner_User_Info['id']?>" type="text" class="write_msg" placeholder="Type a message" />
              <button type="submit" class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
            </form>
            <?php }else{ ?>
            <input type="text" class="write_msg" value="Click on left for Chat with sellers" disabled >
            <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>

    <script src="../includes/jquery/validate.js"></script>
    <script src="../includes/jquery/ajaxform.js"></script>


<script>

<?php 	
	if(isset($_REQUEST['user']) and $_REQUEST['user']!='' ) {
		echo 'LoadMessages('.$ConversationPartner_User_Info['id'].');'; // Load message already send
		echo 'var myVar = setInterval("LoadMessages('.$ConversationPartner_User_Info['id'].')", 5000);'; // Load messages all 5 seconds
	}

	echo 'LoadInboxUsers();'; // Load Inbox users

	if(isset($_REQUEST['user']) and $_REQUEST['user']!='' ) {
		// Activate the conversation between seller and customer
		echo '
			$(\'#MessageFrm_'.$ConversationPartner_User_Info['id'].'\').validate({
				submitHandler: function(form){
					$.post(\'../handler/actions.php?action=sendMessage&user_id='.$ConversationPartner_User_Info['id'].'\', $(\'#MessageFrm_'.$ConversationPartner_User_Info['id'].'\').serialize(), function showInfo(responseData){
						if(responseData==\'success\') {
							document.getElementById(\'MessageFrm_'.$ConversationPartner_User_Info['id'].'\').reset();
							LoadMessages('.$ConversationPartner_User_Info['id'].');
						}
					});
				}
			});
		';
	} 
?>

// Function for to send a message
function LoadMessages(userid){
	$.post('../handler/actions.php?action=GetMessages&user_id='+userid, function(responseData){
		$('.message').html(responseData);
		$('.message').scrolltop($('.message').height());
	});
}

// Function for Load Inbox Users
function LoadInboxUsers(){
	$.post('../handler/actions.php?action=GetInboxUsers', function(responseData){
		$('.allusers').html(responseData);
	});
}

</script>

    </html>
