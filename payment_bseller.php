<?php
error_reporting(0);
session_start();
include("includes/config.php");
require("config.php");
require("classes/block_io.php");
require("classes/gateway1.php");

$user_id = $_SESSION['id'];

$ven = $bdd->query("SELECT * FROM become_seller ");
$vend = $ven->fetch();

$product_price = $vend['price'];

$source2 = "https://blockchain.info/tobtc?currency=USD&value=".$product_price."";
$file1 = file_get_contents($source2);
$btc1 = json_decode($file1, true);

?>
<html>
	<head>
		<title>Markety - Payment Request</title>
		<link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="assets/css/gateway.css" rel="stylesheet" type="text/css">
		<script src="assets/js/jquery-2.1.1.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/gateway.js"></script>
	</head>
	<body>
		<div class="container" style="margin:0 auto;padding:20px;">
			<div class="row">
				<div class="col-sm-3 col-md-3 col-lg-3"></div>
				<div class="col-sm-6 col-md-6 col-lg-6">
				<?php
				$box = new BtcGateway();
				echo $box->create_payment_box_seller(''.$btc1.'');
				?>
				</div>
				<div class="col-sm-3 col-md-3 col-lg-3"></div>
			</div>
		</div>
	</body>
</html>
