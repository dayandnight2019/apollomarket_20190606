<?php 
session_start(); 
include('../includes/config.php');

$admin_use = $bdd->query("SELECT * FROM adminuser");
$admin_user = $admin_use->fetch();

$id = htmlspecialchars(@$_GET['id']);


$source = "https://www.blockchain.com/ticker";
$file = file_get_contents($source);
$btc = json_decode($file, true);

$user_id = htmlspecialchars(@$_SESSION['id']);

$userInfo = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$userInfo->execute(array($user_id));

$rsuserInfo = $userInfo->fetch();

$userSeller1 = $bdd->prepare("SELECT * FROM members WHERE id = ? and vendor='1'");
$userSeller1->execute(array($user_id));

$rsuserSeller1 = $userSeller1->fetch();

$articleInfo = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$articleInfo->execute(array($id));
$rsarticleInfo = $articleInfo->fetch();

if($rsuserInfo['vendor']==2) {
  header('Location: ../members/locked.php');
}


?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?= $admin_user['name_website'] ?> - <?= $rsarticleInfo['title'] ?></title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../includes/css/shop-homepage.css" rel="stylesheet">
    <script src="ckeditor/ckeditor.js"></script>

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="../index.php"><?= $admin_user['name_website'] ?></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

            <?php if(isset($_SESSION['id'])) { ?>



          <?php } ?>

              <form action="../search.php" class="form-inline mt-2 mt-md-0" method="GET">
            <input name="q" class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" required>
            <button style="margin-right:10px;" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
            </form>

              <?php if(!isset($_SESSION['id'])) { ?>


                    <?php if(!isset($_SESSION['member_id'])) { ?>
              <li class="nav-item active">
                <a style="margin-right:10px;" class="btn btn-success" href="../becomeseller.php"><i class="fa fa-plus-square"></i> Become a seller</a>
              </li>
            <li class="nav-item active">
              <a class="nav-link" href="../members/login.php">Log In</a>
            </li>
              <li class="nav-item active">
              <a class="nav-link" href="../members/register.php">Register</a>
                  <?php }else{ ?>
                   <li class="nav-item active">
                <a class="nav-link" href="../admin/index.php">Admin Panel</a>
                </li>


                  <?php } ?>
            </li>
              <?php }else{ ?>

             <?php if($rsuserInfo['vendor']==1 or $rsuserInfo['vendor']==2 or $rsuserInfo['vendor']==3) { ?>
            <li class="nav-item active">
                <a style="margin-right:10px" class="btn btn-success" href="addarticle.php"><i class="fa fa-plus-square"></i> Add Product</a>
              </li>
            <?php }else{ ?>

            <li class="nav-item active">
                <a style="margin-right:10px;" class="btn btn-success" href="../becomeseller.php"><i class="fa fa-plus-square"></i> Become a seller</a>
            </li>

            <?php } ?>

            <li class="nav-item active">
                <a style="margin-right:10px;" class="btn btn-success" href="../ticket.php"><i class="fas fa-life-ring"></i> Support</a>
              </li>


            <?php  if(empty($rsuserInfo['ppicture'])) { ?>
              <li class="nav-item active">
                <img style="border-radius:200px;" src="../members/images/default.png" height="35px" width="35px" />
            </li>
                <?php }else{ ?>

              <li class="nav-item active">
                <img style="border-radius:200px;" src="../members/uploads/<?=$rsuserInfo['ppicture']?>" height="35px" width="35px" />
            </li>
                <?php } ?>
            <li class="nav-item active">
                <a class="nav-link" href="../members/member.php?username=<?=$_SESSION['username']?>&id=<?=$_SESSION['id']?>&pn=1"><span class="badge badge-success"><?=$_SESSION['username']?></span></a>
            </li>
              <?php

                    $cart_number = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0'");
                    $cart_number->execute(array($user_id));
                    $cart_numbers = $cart_number->rowCount();

              ?>
              <li class="nav-item active">
                <a class="nav-link" href="../members/cart.php"><i class="fa fa-shopping-cart"> (<?=$cart_numbers?>)</i></a>
                </li>
              <li class="nav-item active">
                <a class="nav-link" href="../messages/message.php"><i class="fa fa-envelope"> </i></a>
                </li>

             <li class="nav-item active">
                <a class="nav-link" href="../members/logout.php"><i style="color:red;" class="fa fa-power-off"></i></a>
            </li>
              <?php } ?>
              <li class="nav-item active">
                  <a class="nav-link" ><i style="color:white;" class="btcHeader nav-link fa fa-btc"></i> : <?php  echo $btc["USD"]["last"]; ?> $</a>

          </ul>
        </div>
      </div>
    </nav>
