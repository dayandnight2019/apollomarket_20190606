<?php include('header.php');

if(empty($id)) {
  header('Location: ../index.php');
}



$showSeller = $rsarticleInfo['user_id'];

$id_arti = $rsarticleInfo['id'];

$vend = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$vend->execute(array($rsarticleInfo['user_id']));
$vendor = $vend->fetch();

$checkout_button = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0'");
$checkout_button->execute(array($user_id));
$checkoutButton =  $checkout_button->rowCount();

$cat = $bdd->query("SELECT * FROM categories");

// DELETE PRODUCT BUTTON
if(isset($_POST['deleteProduct'])) {
	$id_rdv = htmlspecialchars($_POST['id']);

	$delete_article = $bdd->prepare("DELETE FROM articles where id = ?");
	$delete_article->execute(array($id_rdv));

	$delete_success = '<div class="alert alert-success">Product deleted with success !</div>';
}

// ADD TO CART BUTTON

if(isset($_POST['addCartBtn'])) {

	$order_article = substr(md5(rand().microtime(true)), 0, 24);
	$pass = substr(md5(rand().microtime(true)), 0, 24);
    //$order_article = rXand(999,50000);
    //$pass = rXand(999,50000);

    $token = hash('sha256', $pass);

    $insert_cart = $bdd->prepare("INSERT INTO orders (user_from,for_id,token,pass,article_id,order_article,date_added) VALUES(?,?,?,?,?,?,NOW())");
    $insert_cart->execute(array($user_id,$showSeller,$token,$pass,$id_arti,$order_article));

    echo"<script>window.location 'article.php'</script>";

    $success = '<div class="alert alert-success">Product added !</div>';

}

?>

    <!-- Page Content -->
    <div class="container">
    <?php echo @$success;
          echo @$delete_success;
    ?>
      <div class="row">
        <!--<div class="col-lg-3">
          <h6 class="my-4">Categories</h6>
          <div class="list-group">
        // while($row = mysqli_fetch_assoc($cat)) {

                    //$id_cate =  $row['id'];
                    //$catego =   $row['category'];

              //echo'<a href="../category/category.php?cat='.$id_cate.'" class="list-group-item">'.$catego.'</a>';

            }
          </div>
        </div>-->
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <?php

								// Blockchain script for to show the price in BTC
                $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$rsarticleInfo['price']."";
                $file1 = file_get_contents($source2);
                $btc1 = json_decode($file1, true);
            ?>

          <div class="card mt-4">
            <!--<img class="card-img-article img-fluid" src="uploads/#" alt="">-->
            <div class="card-body">
              <div class="col-lg-12">
            <img style="margin-left:auto;margin-right:auto;display:block;" src="uploads/<?= $rsarticleInfo['ppicture'] ?>" width="200px">
            <br>
            <h4 style="background-color:#000;color:white;padding:20px;" class="card-title text-center"><?=$rsarticleInfo['title']?></h4>
            </div>
              <br>
              <div class="col-lg-12">
              <h6>Description</h6>
              <hr>
              <p class="card-text"><?=$rsarticleInfo['description']?></p>

            </div>
              </div>

          </div>
              <br />
          <!-- /.card -->
          <!-- /.card -->

        </div>
        <div class="col-lg-3">
          <div class="card mt-4">
          <div class="card-body">
            <h4 class="text-center"><?=$rsarticleInfo['price']?> <i class="fa fa-dollar"></i></h4>
              <h4 class="text-center"><?=$btc1?> <i class="fa fa-btc"></i></h4>
              <br>
              <h6 class="text-center">Shipping : <?= $rsarticleInfo['shipping_country'] ?></h6>
                <?php if(!isset($_SESSION['id'])) { ?>
                    <a href="../members/register.php" class="buyBtnArticle btn btn-dark btn-block">Register now</a>
                <?php }else{

                            if($_SESSION['id']===$rsarticleInfo['user_id']) {
                       ?>
                      <a href="editarticle.php?id=<?=$id?>" class="buyBtnArticle btn btn-dark btn-block">Edit Product</a>
                        <br>
                    <form method="POST" action="article.php?id=<?=$id?>">
                        <input  type="hidden" name="id" value="<?=$id?>">
                        <button type="submit" name="deleteProduct"  class=" btn btn-danger btn-block">Delete Product</button>
                    </form>

                <?php
                            }else{


                      if($checkoutButton >= 1) {

                ?>

                 <a href="../members/cart.php" class="buyBtnArticle btn btn-dark btn-block">Checkout</a>



                <?php

              }else{


                ?>

                <form action="article.php?id=<?=$id?>" method="POST">
                  <button name="addCartBtn" class="buyBtnArticle btn btn-dark btn-block" type="submit">Add to cart</button>
              </form>

                <?php

                      }
                  }
              }

                ?>

        </div>
        <div class="card-footer">
        <?php if(empty($vendor['ppicture'])) { ?>
         <p> Vendor : <img style="border-radius:200px;" src="../members/images/default.png" height="25px" width="25px"/> <a href="../members/member.php?username=<?=$vendor['username']?>&id=<?=$vendor['id']?>&pn=1"><?=$vendor['username']?></a></p>
        <?php }else{ ?>
         <p>Vendor : <img style="border-radius:200px;" src="../members/uploads/<?=$vendor['ppicture']?>" height="30px" width="30px" /> <a href="../members/member.php?username=<?=$vendor['username']?>&id=<?=$vendor['id']?>&pn=1"><?=$vendor['username']?></a></p>
       <?php } ?>

       <?php if(!isset($_SESSION['id'])) { ?>

         <a href="../messages/message.php?user=<?=$vendor['username']?>" ><button class="btn btn-black btn-block" disabled>Register for to contact</button></a>
        <?php }else{

            if($_SESSION['id']===$rsarticleInfo['user_id']){

          ?>

       <button class="btn btn-dark btn-block" disabled>Contact Seller</a>
            <?php
          }else{
             ?>
        <a class="btn btn-dark btn-block" href="../messages/message.php?user=<?=$vendor['username']?>" >Contact Seller</a>
        <?php  }

      }?>

     </div>
          </div>
        </div>
        <!-- /.col-lg-9 -->

      </div>

    </div>
    <!-- /.container -->



    <!-- Bootstrap core JavaScript -->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
