<?php include('header.php');

if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
}

$id_article =  htmlspecialchars($_GET['id']);

$arti_user = $bdd->prepare("SELECT * FROM articles where id = ? and user_id = ?");
$arti_user->execute(array($id_article,$user_id));
$article_user = $arti_user->fetch();

if($_SESSION['id']!=$article_user['user_id']) {
    header('Location: ../404.php');
}

if($rsuserInfo['vendor']==0) {
    header('Location: ../404.php');
}

$select = $bdd->prepare("SELECT * FROM articles where id = ? and user_id = ?");
$select->execute(array($id_article,$user_id));

// show the data of product selected
if($select->rowCount()==1){

    $result = $select->fetch();

      $title =  $result['title'];
      $description =  $result['description'];
      $category =  $result['category'];
      $price =  $result['price'];
      $ppicture = $result['ppicture'];


    if(isset($_POST['updArticleBtn'])) {

            if( !empty($_POST['title']) and !empty($_POST['description']) and !empty($_POST['category']) and !empty($_POST['price'])) {

      $title = htmlspecialchars($_POST['title']);
      $description = $_POST['description'];
      $category = htmlspecialchars($_POST['category']);
      $price = htmlspecialchars($_POST['price']);
      $country = htmlspecialchars($_POST['country']);


      $update_prod = $bdd->prepare("UPDATE articles SET title = ?,category = ?,description = ?,price = ?,shipping_country = ? where id = ? and user_id = ?");
      $update_prod->execute(array($title,$category,$description,$price,$country,$id_article,$user_id));


                $success = "<div class='alert alert-success'>Updated Info successfully !</div>";


            }else{



          }

        }

      }


// Update picture script request
    if(isset($_POST['updPicBtn'])) {


                $filename = $_FILES['ppicture']['name'];
                $tmp_name = $_FILES['ppicture']['tmp_name'];
                $filename = rand(9999,10000).date('Ymdhis').$filename;
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
                // verification of file extension
                 if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                    && $imageFileType != "gif" ) {

                    $errors_file = "<div class='alert alert-danger'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                    $uploadOk = 0;

                    }else{

                     $uploadOk = 1;
                     move_uploaded_file($tmp_name, 'uploads/'.$filename);

                     $update_pic = $bdd->prepare("UPDATE articles SET ppicture = ? where id = ? and user_id = ?");
                     $update_pic->execute(array($filename,$id_article,$user_id));

                     $success_pic = "<div class='alert alert-success'>Picture Updated !</div>";


                        }


                 }


  $options = '';
  $categories = $bdd->query("SELECT * FROM categories");
  // show all categories
  while( $rs = $categories->fetch()){
      $categoChoose = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
      $categoChoose->execute(array($id_article));
  while($rs1 = $categoChoose->fetch()) {
    $options .= '<option value="'.$rs['id'].'"'.(($rs1['category'] === $rs['id']?'selected':'')).'>'.$rs['category'].'</option>';
      }
    }

?>


<div class="container">
  <br>
    <h1 class="text-center"><i class="fas fa-edit"></i> | Edit product</h1>
<form method="POST" enctype="multipart/form-data">
    <?php echo @$success; ?>
    <?php echo @$success_pic; ?>
    <?php echo @$errors; ?>
    <?php echo @$errors_file; ?>
    <div class="form-group">
        <label for="inputEmail">Title</label>
              <div class="form-label-group">
                <input type="text"  class="form-control" name="title" value="<?=((isset($title) )?$title:'')?>"   >
              </div>
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Description</label>
            <textarea id="editor1"  class="form-control" name="description" ><?=((isset($description) )?$description:'')?></textarea>
              </div>
    </div>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Category</label>
            <select class="form-control" name="category" >
                <?php

                    echo $options;

                ?>

            </select>
              </div>
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Price ($)</label>
            <input type="number" class="form-control" name="price"  value="<?=((isset($price) )?$price:'')?>"   />
        </div>
    </div>

    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Shipping from</label>
            <select  class="form-control" name="country"  required="required">
              <?php $countryShow = mysqli_query($connection, "SELECT * FROM apps_countries");
              while($row = mysqli_fetch_array($countryShow)) {
                  $articleCountry = mysqli_query($connection, "SELECT * FROM articles WHERE id='$id_article'");
              while($row1 = mysqli_fetch_array($articleCountry)) {
                ?>
                <option value="<?= $row['country_name'] ?>" <?= (($row1['shipping_country'] === $row['country_name']?'selected':'')) ?>><?=  $row['country_name'] ?></option>
                <?php
              }
              }
              ?>
            </select>
        </div>
    </div>

    <button class="btn btn-dark btn-block" name="updArticleBtn"><i class="fas fa-wrench"></i> Update info article</button>


    <div class="form-group">
        <img class="card-img-top" style="margin-top:20px;max-height: 300px;" src="uploads/<?=$ppicture?>" />
        <div class="form-label-group">
            <label for="inputEmail">Picture</label>
            <input type="file" class="form-control" name="ppicture"  />
        </div>
    </div>

    <button class="btn btn-dark btn-block"  name="updPicBtn"><i class="fas fa-wrench"></i> Update Picture</button>
    <br>

</form>
</div>

    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
