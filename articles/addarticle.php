<?php include('header.php');


if(!isset($_SESSION['id'])) { header('Location: /login.php'); }
if($rsuserInfo['vendor']==0) { die('You do not have permission to access this resource. This incident has been logged.'); }

$errors = ''; $success = ''; $title=''; $description=''; $description_sh=''; $category=''; $price=''; $options = '';
$user_id = $_SESSION['id'];
	

if(isset($_POST['addArticleBtn']) ){
	$title = htmlspecialchars($_POST['title']);
	$description =  $_POST['description'];
	$category = htmlspecialchars($_POST['category']);
	$country = htmlspecialchars($_POST['country']);
	$price = htmlspecialchars($_POST['price']);

	if(!empty($title) and !empty($description) and !empty($category) and !empty($price) and !empty($country)){
		$filename = $_FILES['ppicture']['name'];
		$imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));
		$tmp_name = $_FILES['ppicture']['tmp_name'];
		$filename = md5(rand().microtime(true)).'.'.$imageFileType;

		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
			$errors = "<div class='alert alert-danger'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
		} else {
			move_uploaded_file($tmp_name, 'uploads/'.$filename);
			$insert = $bdd->prepare("INSERT INTO articles (user_id, title,ppicture,category,description,price,shipping_country,date_added) VALUES(?,?,?,?,?,?,?,NOW())");
			$insert->execute(array($user_id,$title,$filename,$category,$description,$price,$country));
			$success = "<div class='alert alert-success'>Article added with success ! return to <a href='../index.php'>Return to index</a></div>";
		}
    }
}



$categories = $bdd->query("SELECT * FROM categories");

while($rs = $categories->fetch()){
	$options .= '<option value="'.$rs['id'].'">'.$rs['category'].'</option>';
}


//This section is unused
	/*// Show number of product for to lock if the number of product is == to 6
	$produ_row = $bdd->prepare("SELECT * FROM articles WHERE user_id = ?");
	$produ_row->execute(array($rsuserSeller1['id']));
	$produ_rows = $produ_row->rowCount();*/

?>


<div class="container">
  <br>

    <h1 class="text-center"><i class="fas fa-cart-plus"></i> | Add product</h1>

<form method="POST" enctype="multipart/form-data">
    <?php echo @$success; ?>
    <?php echo @$errors; ?>
    <?php echo @$errors_file; ?>
    <br>
    <div class="form-group">
        <label for="inputEmail">Picture</label>
              <div class="form-label-group">
                <input type="file"  class="form-control" name="ppicture"  required="required" >
              </div>
    </div>
    <div class="form-group">
        <label for="inputEmail">Title</label>
              <div class="form-label-group">
                <input type="text"  class="form-control" name="title" placeholder="Title of product"  required="required">
              </div>
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Description</label>
            <textarea id="editor1" style="min-height:200px;"  class="form-control" name="description" placeholder="Enter the description of your product"  required="required"></textarea>
              </div>
    </div>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Category</label>
            <select class="form-control" name="category" >
                <?php
                    echo $options;
                ?>
            </select>
              </div>
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Price ($)</label>
            <input type="number" class="form-control" name="price"  required="required"  />
        </div>
    </div>
    <div class="form-group">
        <div class="form-label-group">
            <label for="inputEmail">Shipping from</label>
            <select  class="form-control" name="country"  required="required">
			<?php 
				$countryShow = $bdd->query("SELECT * FROM apps_countries");
				while($row = $countryShow->fetch()) {
					echo '<option value="'.$row['country_name'].'"><'.$row['country_name'].'</option>';
				}
			?>
            </select>
        </div>
    </div>
	<button class="btn btn-dark btn-block" name="addArticleBtn"><i class="fas fa-plus-square"></i> Add product</button>
	<br>

</form>
</div>
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
