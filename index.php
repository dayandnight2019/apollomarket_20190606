<?php include('header.php');
$cat = $bdd->query("SELECT * FROM categories");

$sql = "SELECT * from  categories";
$query = $bdd -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);

//$result = $bdd->query($cat);
//$result = $cat->fetchAll();
//echo'<pre>';print_r($results);die();
$article_id =  htmlspecialchars(@$_GET['article']);

$produ = $bdd->prepare("SELECT * FROM articles WHERE id= ?");
$produ->execute(array($article_id));
$produc = $produ->fetch();

$order = $bdd->prepare("SELECT * FROM orders WHERE user_from= ? and done='0'");
$order->execute(array($user_id));
$orders = $order->rowCount();

// Delete product button

if(isset($_REQUEST['deleteProduct'])) {
	//TODO: Security on this.
	//TODO: Find out why this is on multiple pages.
	die('This feature currently unavailable. [h3JVUVx2MqDqa1MI]');
	/*$id_rdv = htmlspecialchars($_POST['id']);
	$delete_article = $bdd->prepare("DELETE FROM articles where id='$id_rdv'");
	$delete_article->execute(array($id_rdv));
	$delete_success = '<div class="alert alert-success">Product delete with success !</div>';*/
}


    // Add to cart button

if(isset($_REQUEST['addCartBtn'])) {
	//TODO: Find out why this is on multiple pages.
	die('This feature currently unavailable. [aZbALnBdgMcad2e4]');
	/*$order_article = rand(999,50000);
	$insert_cart = $bdd->prepare("INSERT INTO orders (user_from,article_id,order_article, date_added) VALUES(?,?,?,NOW())");
	$insert_cart->execute(array($user_id,$article_id,$order_article));
	echo"<script>window.location 'index.php'</script>";
	$success = '<div class="alert alert-success">Product added to shopping cart !</div>';*/
}

?>
    <!-- Page Content -->
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
        <h6 class="my-4"></h6>
          <ul class="list-group list-unstyled">
			<li><a class="list-group-item active" style="color:white;background-color:black!important"><i class="fas fa-folder-open"></i> Categories</a></li>
					
			<?php 
				while($row = $cat->fetch()) {
					$cat_number = $bdd->prepare("SELECT * FROM articles WHERE category = ?");
					$cat_number->execute(array($row['id']));
					$row1 = $cat_number->rowCount();
					
					?>
						<li>
							<?php
								if($row['parent_id'] == 0)
								{
									?>
										<a class="accordion-toggle collapsed toggle-switch list-group-item" data-toggle="collapse" href="#submenu-<?= $row['id']?>">                            
											<span class="sidebar-title"><?php echo $row['category']; ?></span>
											<i class="fa fa-caret-right"></i>
											<p style="font-size:12px;color:white;display:inline-flex;border-radius:50px;background-color:#343a40;float:right;padding:4px;"><?= $row1; ?></p>
										</a>
										<ul id="submenu-<?= $row['id']?>" class="panel-collapse collapse panel-switch list-unstyled" role="menu">
											<?php
												foreach($results as $rrr)
												{
													if($rrr->parent_id == $row['id'])
													{
														?>
															<li><a href="#"><?= $rrr->category?></a></li>															
														<?php														
													}
												}
											?>
										</ul>
									<?php
								}
							?>
							<!--<a href="../category/category.php?cat='.$row['id'].'&pn=1" class="accordion-toggle collapsed toggle-switch list-group-item"><?php echo $row['category']; ?><p style="font-size:12px;color:white;display:inline-flex;border-radius:50px;background-color:#343a40;float:right;padding:4px;"><?= $row1; ?></p></a>-->
						</li>
					
					<?php
				}
			?>
          </ul>
          <h6 class="my-4"><i class="fas fa-crown"></i> Top seller</h6>
			<?php 
				$seller_top = $bdd->query("SELECT * FROM members WHERE vendor='1' ORDER BY sales DESC LIMIT 0,5");
				while($row1 = $seller_top->fetch()) {
					$id_seller = $row1['id'];
					$seller_name = $row1['username'];
					$seller_pic = $row1['ppicture'];
					$sales_total = $row1['sales'];
					echo'<div class="col-lg-4 col-sm-6 text-center mb-4">';
					echo'<a href="members/@'.$seller_name.'-'.$id_seller.'-1.html"><img style="border-radius:200px;border:2px solid #ffc107;" src="../members/uploads/'.$seller_pic.'"  height="50px"/></a>';
					echo'</div>';

				}
			?>
          <h6 class="my-4"></h6>
          <img src="members/images/marketyBtc.png" height="40px" />

          <h6 class="my-4"></h6>
          <h5><a href="privacy.html" ><span class="badge badge-dark"><i class="fas fa-info-circle"></i> Terms and conditions</span></a></h5>

        </div>
        <!-- /.col-lg-3 -->
        <div class="col-lg-9">
            <?php echo @$success; ?>
            <?php echo @$delete_success; ?>
          <div id="carouselExampleIndicators" class="carousel slide my-4 carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <img class="d-block img-fluid" src="includes/images/marketyV3.jpg" alt="First slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
            <h3 style="background-color:black;color:white;padding:10px" class="text-center"><i class="fas fa-shopping-basket"></i> Last products</h3>
            <hr class="mb-4">
          <div class="row">
              <?php

            $product = $bdd->query("SELECT * FROM articles ORDER BY id DESC LIMIT 0,6");

            // Show only 6 last products

            while($row = $product->fetch()) {

                $user_id = @$_SESSION['id'];

                $id_article =   $row['id'];
                $pic_article = $row['ppicture'];
                $user_from = $row['user_id'];
                $tit_article = $row['title'];
                $descri_article = $row['description'];
                $pri_article = $row['price'];
                $shippinCountry = $row['shipping_country'];

                $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$pri_article."";
                $file1 = file_get_contents($source2);
                $btc1 = json_decode($file1, true);

				$memb = $bdd->prepare("SELECT * FROM members WHERE id= ?");
				$memb->execute(array($user_from));

				  // Show vendors of the last products

				while($row1 = $memb->fetch()) {

					$id_membe = $row1['id'];
					$user_membe = $row1['username'];
					$pic_membe = $row1['ppicture'];

					echo'<div class="col-lg-4 col-md-6 mb-4">';
					echo'<div class="card h-100">';
					echo'<a href="../articles/article.php?id='.$id_article.'"><img class="card-img-top" src="../articles/uploads/'.$pic_article.'" alt=""  height="500px" width="500px" ></a>';
					echo'<div class="card-body">';
					echo'<h6 class="card-title text-center">';
					echo '<a href="../articles/article.php?id='.$id_article.'">'.$tit_article.'</a>';
					echo'</h6>';
					echo'<h6 class="text-center">'.$pri_article.' <i class="fa fa-dollar"></i> / '.$btc1.' <i class="fa fa-btc"></i></h6>';
					if(empty($pic_membe)) {
						echo'<h6 class="text-center">Shipping : '.$shippinCountry.'</h6>';
						echo'<h6 class="text-center"> Vendor : <img style="border-radius:200px;" src="../members/images/default.png" height="25px" width="25px"/> <a href="../members/@'.$user_membe.'-'.$id_membe.'-1.html">'.$user_membe.'</a></p>';
					} else {
						echo'<h6 class="text-center">Shipping : '.$shippinCountry.'</h6>';
						echo'<h6 class="card-text text-center">Vendor : <img style="border-radius:200px;" src="../members/uploads/'.$pic_membe.'" height="25px" width="25px" /> <a href="../members/@'.$user_membe.'-'.$id_membe.'-1.html">'.$user_membe.'</a></p>';
					}
					echo'</div>';

					//Here insert the card footer in bloc note
					echo'</div>';
					echo'</div>';

				}

            }
        ?>


          </div>
          <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

      </div>
      <!-- /.row -->

    </div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="includes/vendor/jquery/jquery.min.js"></script>
    <script src="includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
