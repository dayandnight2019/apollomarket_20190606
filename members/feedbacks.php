<?php include('header.php');

if(!isset($_SESSION['id'])) {
  header('Location: ../index.php');
}

?>

<div class="container">
  <br>
    <h2 class="text-center">Your feedback</h2>
    <hr>
<div class="card mb-5">
            <div class="card-header">
              <i class="fa fa-btc"></i> |
              feedback Seller</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Seller</th>
                      <th>Article</th>
                      <th>feedback</th>
                    </tr>
                  </thead>
                  <tbody>
             <?php
                // show feedback of user_id
                $article_order = $bdd->prepare("SELECT * FROM feedback WHERE user_id = ? AND good='0'");
                $article_order->execute(array($user_id));
                      while($row = $article_order->fetch()) {

                          $id_art =    $row['id'];
                          $id_article = $row['article_id'];

                  // Show seller of feedback
                $arti_fee = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
                $arti_fee->execute(array($id_article));
                        while($row1 = $arti_fee->fetch()) {

                            $id_order = $row1['id'];
                            $customer_id = $row1['user_id'];
                            $name_article = $row1['title'];

                  // show cateogry of product who is order
                $show_cate = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $show_cate->execute(array($customer_id));
                        while($row2 = $show_cate->fetch()) {

                            $seller_feedback = $row2['username'];

                      echo'<tr>';
                      echo'<td>'.$id_art.'</td>';
                      echo'<td>'.$seller_feedback.'</td>';
                      echo'<td>'.$name_article.'</td>';
                      echo'<td><a class="btn btn-dark" href="feedback.php?id='.$id_art.'">Give a note</a></td>';
                      echo'</tr>';


                            }
                          }
                        }

            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

                  </div>


    <!-- Bootstrap core JavaScript-->
    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="../admin/js/demo/datatables-demo.js"></script>
    <script src="../admin/js/demo/chart-area-demo.js"></script>
