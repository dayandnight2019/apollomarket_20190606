<?php 
$pg = basename(substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],'.'))); // get file name from url and strip extension
?>
<div class="list-group">
	<a href="editpic.php" class="list-group-item <?php if($pg=='editpic'){?>active<?php }?>"><i class="fas fa-image"></i> Edit Picture</a>
	<a href="editprofile.php" class="list-group-item <?php if($pg=='editprofile'){?>active<?php }?>"><i class="fas fa-info-circle"></i> Edit Infos</a>
	<a href="pgpkey.php" class="list-group-item <?php if($pg=='pgpkey'){?>active<?php }?>"><i class="fas fa-key"></i> PGP Key</a>
	<a href="walletreturn.php" class="list-group-item <?php if($pg=='walletreturn'){?>active<?php }?>"><i class="fas fa-wallet"></i> Wallet Return</a>
	<a href="editpwd.php" class="list-group-item <?php if($pg=='editpwd'){?>active<?php }?>"><i class="fas fa-key"></i> Edit Password</a>
</div>