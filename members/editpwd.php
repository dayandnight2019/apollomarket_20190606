<?php include('header.php');

 if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
 }

$select = $bdd->prepare("SELECT * FROM members where id = ?");
$select->execute(array($user_id));
// Edit password already exists
if($select->rowCount()==1){
	$result = $select->fetch();
	$password = $result['password'];
	if(isset($_POST['pwdBtn'])) {
		if ($_POST["password"] === $_POST["confirmPwd"]) {
			if( !empty($_POST['password'])) {

				$password = htmlspecialchars($_POST['password']);
				$password = hash('sha256', $password);
				$update = $bdd->prepare("UPDATE members SET password = ? where id = ?");
				$update->execute(array($password,$user_id));
				$success = "<div class='alert alert-success'>Updated Password successfully</div>";
			}else{
				$errors = "<div class='alert alert-danger'>The passwords are not the same!</div>";
			}
		}
	}
}
?>
<!-- Page Content -->
    <div class="container">

      <!-- Team Members Row -->
      <div class="row">

        </div>
		<br>
        <div style="float:left;" class="col-lg-4">
          <h1 class="my-4"></h1>
			<div class="list-group">
				<a href="editpic.php" class="list-group-item"><i class="fas fa-image"></i> Edit Picture</a>
				<a href="editprofile.php" class="list-group-item"><i class="fas fa-info-circle"></i> Edit Infos</a>
				<a href="pgpkey.php" class="list-group-item"><i class="fas fa-key"></i> PGP Key</a>
				<?php
					if($result['vendor'] > 0 )
					{
						?>
							<a href="wallet.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet</a>						
						<?php
					}
					else
					{
						?>
						<a href="walletreturn.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet Return</a>						
						<?php
					}
				?>
				<a href="editpwd.php" class="list-group-item active"><i class="fas fa-key"></i> Edit Password</a>
			</div>
        </div>

    <div class="col-lg-12 col-sm-11 mb-1">

            <form style="float:right;margin-top:30px;" method="POST">
                  <?php echo @$errors; ?>
                  <?php echo @$success; ?>
                <label for="inputUsername">Password</label>
                <input type="password" id="inputEmail" class="form-control-members" name="password" placeholder="Password"  required="required" autofocus="autofocus">
                <label for="inputUsername">Confirm Password</label>
                <input type="password" id="inputUsername" class="form-control-members" name="confirmPwd" placeholder="Confirm Password" required="required">
            <button style="margin-top: 20px;" name="pwdBtn" class="btn btn-dark btn-block"><i class="fas fa-wrench"></i> Update Password</button>
          </form>
    </div>
</div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
