<?php include('header.php');

$id = htmlspecialchars($_GET['id']);

if(!isset($id)) {
  header('Location: ../404.html');
}

if(empty($id)) {
  header('Location: ../404.html');
}

$feedback_sup = $bdd->prepare("SELECT * FROM feedback WHERE id = ?");
$feedback_sup->execute(array($id));
$feedback_sup_me = $feedback_sup->fetch();

$good_fee = $feedback_sup_me['good'];

if($good_fee >= 1) {
  header('Location: ../index.php');
}

$articleid = $feedback_sup_me['article_id'];

$article_feed = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$article_feed->execute(array($articleid));
$article_feedback = $article_feed->fetch();

$seller_see = $article_feedback['user_id'];

$seller_feed = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$seller_feed->execute(array($seller_see));
$seller_feedback = $seller_feed->fetch();

if(isset($_POST['addFeedGBtn'])) {

    $text_feedback = htmlspecialchars($_POST['text_feedback']);

    $update_feedG = $bdd->prepare("UPDATE feedback SET good='a', text_feedback = ? where id = ?");
    $update_feedG->execute(array($text_feedback,$id));

    $success_G = "<div class='alert alert-success'>Your feedback is send !</div>";


}

if(isset($_POST['addFeedNGBtn'])) {

  $text_feedback = htmlspecialchars($_POST['text_feedback']);

  $update_feedNG = $bdd->prepare("UPDATE feedback SET good='b', text_feedback = ? where id = ?");
  $update_feedNG->execute(array($text_feedback,$id));

  $success_NG = "<div class='alert alert-success'>Your feedback is send !</div>";

}

?>
<div class="container">

    <div class="col-lg-10 ">
        <h3 class="text-center">Feedback</h3>
    <form method="POST">
                  <?php echo @$success_NG; ?>
                  <?php echo @$success_G; ?>
                <label for="inputUsername">Username</label>
                <input style="background-color:#f1f1f1;cursor:not-allowed;" type="text" name="username" class="form-control-members" value="<?=$seller_feedback['username']?>" disabled>
                <label for="inputUsername">Article</label>
                <input style="background-color:#f1f1f1;cursor:not-allowed;" type="text" name="article" class="form-control-members" value="<?=$article_feedback['title']?>" disabled>
                <label for="inputUsername">Why good or not good ?</label>
                <textarea style="min-height:300px;" class="form-control-members" name="text_feedback" ></textarea>
            <input style="margin-top: 20px;" type="submit" name="addFeedGBtn" class="btn btn-dark btn-block" value="Good Product/Seller" >
            <input style="margin-top: 20px;" type="submit" name="addFeedNGBtn" class="btn btn-danger btn-block" value="Bad Product/Seller" >
          </form>
    </div>

</div>
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
