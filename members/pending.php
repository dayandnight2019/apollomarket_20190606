<?php include('header.php');

if(!isset($_SESSION['id'])) {
    header('Location: login.php');
}



if(isset($_POST['done2Btn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $update_order =  $bdd->prepare("UPDATE orders SET sent='1', done='2' WHERE id = ?");
    $update_order->execute(array($id_rdv));

    echo"<script>window.location 'pending.php'</script>";

    $update = "<div class='alert alert-success'>Updated order !</div>";

}



?>

<div class="container">
    <h2 class="text-center">Your orders</h2>
<div class="card mb-3">
    <?php echo @$update; ?>
            <div class="card-header">
              <i class="fa fa-btc"></i> |
              Your Orders</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Order Id</th>
                      <th>Customer</th>
                      <th>Product</th>
                      <th>Price</th>
                      <th>Satus</th>
                      <th>Payment</th>
                    </tr>
                  </thead>
                  <tbody>
             <?php
                // show articles of user_id
                $article_order = $bdd->prepare("SELECT * FROM articles WHERE user_id = ?" );
                $article_order->execute(array($user_id));

                      while($row = $article_order->fetch()) {

                          $id_art =    $row['id'];
                          $id_user_art = $row['user_id'];
                          $title_art = $row['title'];
                          $id_cate_art = $row['category'];
                          $price_art = $row['price'];



                  // Show orders where user_id = article_id in "orders" table
                $my_order = $bdd->prepare("SELECT * FROM orders WHERE article_id = ?");
                $my_order->execute(array($id_art));
                        while($row1 = $my_order->fetch()) {

                            $id_order = $row1['id'];
                            $customer_id = $row1['user_from'];
                            $order_id = $row1['order_article'];
                            $done_status = $row1['done'];
                            $send_status = $row1['send'];
                            //$keys_codes = $row1['keys_code'];
                  // Show member who order this product
                $customer_order = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $customer_order->execute(array($customer_id));
                        while($row1 = $customer_order->fetch()) {

                            $customer_user = $row1['username'];
                            $customer_email = $row1['email'];
                  // show cateogry of product who is order
                $show_cate = $bdd->prepare("SELECT * FROM categories WHERE id = ?");
                $show_cate->execute(array($id_cate_art));
                        while($row2 = $show_cate->fetch()) {

                            $cate_art = $row2['category'];

                      echo'<tr>';
                      echo'<td>'.$order_id.'</td>';
                      echo'<td>'.$customer_user.'</td>';
                      echo'<td>'.$title_art.'</td>';
                      echo'<td>'.$price_art.'</td>';

                    if($done_status==0) {
                      echo'<td><span class="badge badge-info">Order pending</span></td>';
                    }else{

                          if($done_status==1){
                            ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_order ?>">
                    <?php

                    echo'<td><button type="submit" name="done2Btn" class="btn btn-dark">Order sent</button></td>';

                    ?>
                  </form>
                  <?php
                          }
                          if($done_status==2){
                      echo'<td><span class="badge badge-success">Order Complete</span></td>';
                        }

                      }
                    if($send_status==0) {



                            echo'<td><span class="badge badge-info" class="btn btn-success">Waiting Payment</span></td>';

                    }else{
                        if($send_status==1){
                      echo'<td><span class="badge badge-success">Admin to the payment</span></td>';
                            }else{
                              echo'<td><span class="badge badge-success">You have received the payment</span></td>';
                            }
                      echo'</tr>';

                              }
                            }
                          }
                        }
                      }
            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>


    <!-- Bootstrap core JavaScript-->
    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="../admin/js/demo/datatables-demo.js"></script>
    <script src="../admin/js/demo/chart-area-demo.js"></script>
