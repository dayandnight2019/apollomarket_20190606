<?php include('header.php');

$id = htmlspecialchars(@$_GET['id']);

if(!isset($_SESSION['id'])) {
  header('Location: ../index.php');
}

if(!isset($id)) {
  header('Location: ../404.php');
}

if(empty($id)) {
  header('Location: ../404.php');
}

$ticket_show = $bdd->prepare("SELECT * FROM support WHERE id = ?");
$ticket_show->execute(array($id));
$ticket_shower = $ticket_show->fetch();

$admin_pic = $bdd->query("SELECT * FROM adminuser");
$admin_picture = $admin_pic->fetch();

$adminPic = $admin_picture['ppicture'];

if(isset($_POST['replyBtn'])) {

  $text_ticket_reply = htmlspecialchars($_POST['text_ticket']);

  $insert_reply = $bdd->prepare("INSERT INTO tickets (user_id,support_id,text_support,date_added) VALUES(?,?,?,NOW())");
  $insert_reply->execute(array($user_id,$id,$text_ticket_reply));

  $success_reply = "<div class='alert alert-success'>Reply send successfully !</div>";
}

?>

<div class="container">
  <h5 class="text-center">Reply ticket # <?= $id ?> </h5>
<div style="max-height:500px;overflow-x:hidden;">
<?php  $ticket_usr = $bdd->prepare("SELECT * FROM tickets WHERE support_id = ?");
       $ticket_usr->execute(array($id));
            while($row = $ticket_usr->fetch()) {

                      $ticket_user_id = $row['user_id'];
                      $text_ticket = $row['text_support'];


      $user_ticket = $bdd->prepare("SELECT * FROM members WHERE id = ?");
      $user_ticket->execute(array($ticket_user_id));
            while($row1 = $user_ticket->fetch()) {
                    $username_ticket = $row1['username'];
                    $pic_ticket = $row1['ppicture'];

                    echo'<div class="row feedCustomer">';
                      echo'<div class="col-md-1">';
                    if($row['user_id']==1) {
                        echo'<img style="border-radius:200px;" src="../admin/uploads/'.$adminPic.'" height="50px" >';
                      }else{
                        echo'<img style="border-radius:200px;" src="uploads/'.$pic_ticket.'" height="50px" >';
                      }
                      echo'</div>';
                      echo'<div class="col-md-11">';
                    if($row['user_id']==1) {
                      echo'<p><span class="badge badge-dark">Administrator</span></p>';
                    }else{
                        echo'<p><span class="badge badge-success">'.$username_ticket.'</span></p>';
                      }
                        echo'<p>'.$text_ticket.'</p>';
                      echo'</div>';
                    echo'</div>';

                  }

            }




?>
</div>
<br />
<script src="../ckeditor/ckeditor.js"></script>
<?php echo @$success_reply; ?>
<form method="POST">
  <?php if($ticket_shower['close'] == 1) { ?>
  <textarea style="min-height:200px;" id="editor1" type="text" class="form-control"  disabled></textarea>
  <br />
  <script>
                  CKEDITOR.replace( 'editor1' );
              </script>
  <input class="btn btn-dark btn-block" name="replyBtn" value="Reply to administrator" disabled>
  <?php }else{ ?>
  <textarea style="min-height:200px;" type="text" class="form-control" name="text_ticket" ></textarea>
  <br />
  <input type="submit" class="btn btn-dark btn-block" name="replyBtn" value="Reply to administrator">
<?php } ?>
</form>

</div>


    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
