<?php include('header.php');

 if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
    }


$select = $bdd->prepare("SELECT * FROM members where id = ?");
$select->execute(array($user_id));

// Edit profile already exists
	if($select->rowCount()==1){

		$result = $select->fetch();

		$username = $result['username'];
		//$email = $result['email'];
    $status = $result['status'];
    $btc_address = $result['btc_address'];


        if(isset($_POST['profileBtn'])) {


            if( !empty($_POST['username'])) {

            $username = htmlspecialchars($_POST['username']);
            //$email = htmlspecialchars($_POST['email']);
            if(isset($_POST['btc_address'])) { $btc_address = htmlspecialchars($_POST['btc_address']); } else { $btc_address = ''; }

                $update = $bdd->prepare("UPDATE members SET username = ?,btc_address = ? where id = ?");
                $update->execute(array($username,$btc_address,$user_id));


                $success = "<div class='alert alert-success'>Updated Info successfully !</div>";

            }else{



        	}

      }


       }

?>

    <!-- Page Content -->
    <div class="container">

      <!-- Team Members Row -->
      <div class="row">

        </div>
		<br>
        <div style="float:left;" class="col-lg-4">

          <h1 class="my-4"></h1>
			<div class="list-group">
				<a href="editpic.php" class="list-group-item"><i class="fas fa-image"></i> Edit Picture</a>
				<a href="editprofile.php" class="list-group-item active"><i class="fas fa-info-circle"></i> Edit Infos</a>
				<a href="pgpkey.php" class="list-group-item"><i class="fas fa-key"></i> PGP Key</a>
				<?php
					if($result['vendor'] > 0 )
					{
						?>
							<a href="wallet.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet</a>						
						<?php
					}
					else
					{
						?>
						<a href="walletreturn.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet Return</a>						
						<?php
					}
				?>
				<a href="editpwd.php" class="list-group-item"><i class="fas fa-key"></i> Edit Password</a>
			</div>
        </div>

    <div style="float:right;margin-top:30px;" class="col-lg-8 col-sm-11 mb-1">
        <script src="../ckeditor/ckeditor.js"></script>
            <form method="POST">
                  <?php echo @$errors; ?>
                  <?php echo @$success; ?>
				  
                <!--<label for="inputUsername">Email</label>
                    <input type="email" id="inputEmail" class="form-control-members" name="email" placeholder="Email address" value="<?=((isset($email) )?$email:'')?>" required="required" autofocus="autofocus">-->
                <label for="inputUsername">Username</label>
                    <input type="text" id="inputUsername"  class="form-control-members" name="username" placeholder="Username" value="<?=((isset($username) )?$username:'')?>">


            <?php if($result['vendor']==0) { ?>

            <?php }else{ ?>

                <label for="inputUsername">Block.io Bitcoin Address</label>
                    <input type="text"  class="form-control-members" name="btc_address" placeholder="Your BTC Address" value="<?=((isset($btc_address) )?$btc_address:'')?>" required="required">

            <?php } ?>
            <button style="margin-top: 20px;"  name="profileBtn" class="btn btn-dark btn-block"><i class="fas fa-wrench"></i> Update infos</button>
          </form>
    </div>
</div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
