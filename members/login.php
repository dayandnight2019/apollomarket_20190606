<?php session_start();
include('../includes/config.php');

if (isset($_SESSION['member_id'])) {
	header('Location: ../admin/index.php');
}

if (isset($_SESSION['id'])) {
	header('Location: ../404.php');
}

// Login SCRIPT Button
if (isset($_POST['loginBtn'])) {
	 if(isset($_POST['captcha'])) {

	$username = htmlspecialchars($_POST['username']);
	$password = htmlspecialchars($_POST['password']);
	$password = hash('sha256', $password);

	if (empty($username) or empty($password)) {
		$errors = '<div class="alert alert-danger">They are fields empty.</div>';
	} else {
		$select = $bdd->prepare("SELECT * FROM members WHERE username = ? and password = ?");
		$select->execute(array($username, $password));
		if ($select->rowCount() == 1) {
			 if ($_POST['captcha'] == $_SESSION['captcha']) {
				$result = $select->fetch();
				$lock_vendor = $result['vendor'];

				$_SESSION['id'] = $result['id'];
				$_SESSION['username'] = $result['username'];

				header('Location: ../index.php');
			 } else {
			 	$errors = '<div class="alert alert-danger">Captcha Not Good !</div>';
			 }
		} else {
			$errors = '<div class="alert alert-danger">username and pasword combination does not match.</div>';
		}
	}
}
}
?>

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>LogIn - Member Panel</title>

	<!-- Bootstrap core CSS-->
	<link href="../includes/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom fonts for this template-->
	<link href="../includes/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

	<!-- Custom styles for this template-->
	<link href="css/sb-admin.css" rel="stylesheet">

</head>

<body class="bg-dark">

	<div class="container">
		<div class="card card-login mx-auto mt-5">
			<div class="card-header">Login</div>
			<div class="card-body">
				<form method="POST">
					<?php echo @$errors; ?>
					<div class="form-group">
						<div class="form-label-group">
							<input type="username" id="inputEmail" class="form-control" name="username" placeholder="Username" required="required" autofocus="autofocus">
							<label for="inputEmail">Username</label>
						</div>
					</div>
					<div class="form-group">
						<div class="form-label-group">
							<input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required="required">
							<label for="inputPassword">Password</label>
						</div>
					</div>
					<div class="form-group">
						<div style="float:left;" class="col-sm-5">
							<img src="captcha.php" />
						</div>
						<div style="float:right;" class="col-sm-7">
							<input class="form-control" type="text" name="captcha" />
						</div>
						<br />
						<br />
					</div>
					<input type="submit" class="btn btn-dark btn-block" value="Log In" name="loginBtn">
					<div class="text-center">
						<a style="text-decoration:none;" class="d-block small mt-3" href="register.php">Need a account ?</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript-->
	<script src="../includes/vendor/jquery/jquery.min.js"></script>
	<script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Core plugin JavaScript-->
	<script src="../includes/vendor/jquery-easing/jquery.easing.min.js"></script>

</body>

</html>