<?php include('header.php');

if(!isset($_SESSION['id'])) {
  header('Location: ../404.php');
}

$usernameTicket = $_SESSION['username'];

?>
<div class="container">
  <br>
  <h2 class="text-center"> <i class="fas fa-ticket-alt"></i> | Your Tickets</h2>
  <hr>
<div class="card mb-5">
    <?php echo @$delete; ?>
   <div class="card-header">
     <i class="fa fa-btc"></i> |
     Tickets from Markety</div>
   <div class="card-body">


     <div class="table-responsive">
       <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
         <thead>
           <tr>
             <th>Ticket ID</th>
             <th>Send to</th>
             <th>Text_ticket</th>
             <th>Reply</th>
           </tr>
         </thead>
         <tbody>
    <?php

       $tickets = $bdd->prepare("SELECT * FROM support WHERE username = ?" );
       $tickets->execute(array($usernameTicket));
             while($row = $tickets->fetch()) {

                 $id_data = $row['id'];
                 $user_id_ticket = $row['username'];
                 $reply_ticket = $row['text_last'];
                 $status_ticket = $row['close'];

             echo'<tr>';
             echo'<td>'.$id_data.'</td>';
             echo'<td>Administrator</td>';
             echo'<td>'.$reply_ticket.'</td>';

            if($status_ticket==1) {

              echo'<td><button class="btn btn-dark" disabled>Reply</button></td>';

                }else{

             echo'<td><a href="ticket.php?id='.$id_data.'"><button class="btn btn-dark">Reply</button></a></td>';

           }
             echo'</tr>';

                       }
   ?>
         </tbody>
       </table>
     </div>
   </div>
 </div>
</div>

    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="../admin/js/demo/datatables-demo.js"></script>
    <script src="../admin/js/demo/chart-area-demo.js"></script>
