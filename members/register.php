<?php session_start(); include('../includes/config.php');

error_reporting(E_ALL);
ini_set('display_errors', '1');

if(isset($_SESSION['member_id'])) {
    header('Location: ../index.php');
}

if(isset($_SESSION['id'])) {
    header('Location: ../index.php');
}


$errors='';
$success='';

// Register script button
    if( isset($_POST['registerBtn']) ) {


    	if ($_POST["password"] === $_POST["confirmPwd"]) {

		    $username = htmlspecialchars($_POST['username']);
        //$email = htmlspecialchars($_POST['email']);
        $password = htmlspecialchars($_POST['password']);

        if( empty($username) or empty($password)) {
            $errors = '<div class="alert alert-danger">Please fill out all fields</div>';

        }else{

        	$select = $bdd->prepare("SELECT * FROM members WHERE username = ? ");
          $select->execute(array($username));
        	if($select->rowCount()==1) {
        		$errors = '<div class="alert alert-danger"> username already exists.</div>';
        	}else{

        		$password = hash('sha256', $password); // add password in sha256

        		$query = $bdd->prepare("INSERT INTO members (username,password,vendor,date_added,ppicture,status,sales,btc_address,earnings) VALUES(?,?,?,'0',NOW(),'','',0,'',0)");
            $query->execute(array($username,$password));

             $success = '<div class="alert alert-success">You are now registered, Log in <a href="login.php">here</a>.</div>';
            	}

            		$username='';
                    //$email='';
            		$password='';
        	}


    	}else{
			$errors ="<div class='alert alert-danger'>Your passwords are not the same.</div>";
			}



  }

?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Register - Markety</title>

    <!-- Bootstrap core CSS-->
    <link href="../includes/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../includes/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-register mx-auto mt-5">
        <div class="card-header">Register</div>
        <div class="card-body">
          <form method="POST">
              <?php echo @$success; ?>
              <?php echo @$errors; ?>
            <div class="form-group">
              <div class="form-row">
              </div>
            </div>
              <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputUsername" class="form-control" name="username" placeholder="Username" required="required">
                <label for="inputUsername">Username</label>
              </div>
            </div>
            <!--<div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="required">
                <label for="inputEmail">Email address</label>
              </div>
            </div>-->
            <div class="form-group">
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required="required">
                    <label for="inputPassword">Password</label>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-label-group">
                    <input type="password" id="confirmPassword" class="form-control" name="confirmPwd" placeholder="Confirm password" required="required">
                    <label for="confirmPassword">Confirm password</label>
                  </div>
                </div>
              </div>
            </div>
			<div class="form-group">
				<div style="float:left;" class="col-sm-5">
					<img src="captcha.php" />
				</div>
				<div style="float:right;" class="col-sm-7">
					<input class="form-control" type="text" name="captcha" />
				</div>
				<br />
				<br />
			</div>
            <input type="submit" name="registerBtn" class="btn btn-dark btn-block" value="Register" />
          </form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../includes/vendor/jquery-easing/jquery.easing.min.js"></script>

  </body>

</html>
