<?php include('header.php');


if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
}


$admin_use = $bdd->query("SELECT * FROM adminuser");
$admin_user = $admin_use->fetch();

$admin_btc = $admin_user['btc_address'];

$order_no = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0' or done='1'");
$order_no->execute(array($user_id));
$order_nope = $order_no->rowCount();

$check = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0' or done='1'");
$check->execute(array($user_id));
$checko = $check->fetch();

$id_order_now = $checko['id'];

$ven = $bdd->prepare("SELECT * FROM orders WHERE user_from='$user_id'");
$ven->execute(array($user_id));
$vend = $ven->fetch();

$show_seller =  $vend['for_id'];
$vend_article_id = $vend['article_id'];

$vendor = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$vendor->execute(array($vend_article_id));
$vendors = $vendor->fetch();

$article_feed = $vendors['id'];

$user_id_article = $vendors['user_id'];

$vendors_user_id = $vendors['user_id'];

$vendors_nam = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$vendors_nam->execute(array($show_seller));
$vendors_name = $vendors_nam->fetch();

$username = $vendors_name['username'];
$email = $vendors_name['email'];
$btc_vendor = $vendors_name['btc_address'];

// Delete button for to delete order

if(isset($_POST['supBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = $bdd->prepare("DELETE FROM orders WHERE id = ?");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'cart.php'</script>";

    $delete = "<div class='alert alert-success'>Product deleted !</div>";

}

?>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <h2>Your orders</h2>
        <p class="alert alert-info">If you want to delete this order, just delete the product.</p>
      </div>
        <?php echo @$success; ?>
      <div class="row">
        <div class="col-md-6 order-md-2 mb-4">
          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted">Your order</span>
            <span class="badge badge-secondary badge-pill"></span>
          </h4>
            <?php echo @$delete; ?>
          <ul class="list-group mb-5">
              <?php


              //List of product choose by customer

              $product_us = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0'");
              $product_us->execute(array($user_id));

              $totalPrice = 0;
              $totalPrice2 = 0;

              // Show product id
              while($row1 = $product_us->fetch()) {

                $id_order = $row1['id'];
                $product_id = $row1['article_id'];

                $product_users = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
                $product_users->execute(array($product_id));

              // Show product title and price
                while($row = $product_users->fetch()) {

                    $product_title = $row['title'];
                    $product_price = $row['price'];

                    // For show price in BTC
                    $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$product_price."";
                    $file1 = file_get_contents($source2);
                    $btc1 = json_decode($file1, true);

               $totalPrice1 = $product_price * 5 / 100; // For change the mount en % just change "5" by your number

              echo'<li class="list-group-item d-flex justify-content-between lh-condensed">';
              echo'<div>';
                echo'<h6 class="my-0">'.$product_title.'</h6>';
              echo'</div>';
              echo'<span class="text-muted">'.$btc1.' <i class="fa fa-btc"></i></span>';
                 ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_order ?>">

                    <?php
                echo'<button name="supBtn" type="submit" class="btn btn-danger">X</button>'; // for delete your cart
                    ?>

                    </form>

                    <?php
                echo'</li>';

                    // For blockchain payment with 5% to administrator or immadiately to seller

                    $totalPrice += $btc1; // totalPrice in BTC
                    $totalPrice2 += $product_price; // totalPrice
                    $totalPrice1 += $product_price; // totalPrice

                            }


                    }



              //Total Price in BTC

                  //$price_user = mysqli_query($connection, "SELECT * FROM articles WHERE id='$product_id'");
                  //while($row2 = mysqli_fetch_assoc($price_user))


                  echo'<li class="list-group-item d-flex justify-content-between">';
                      echo'<span>Total (BTC)</span>';
                      echo'<strong>'.$totalPrice.' <i class="fa fa-btc"></i></strong>';
                    echo'</li>';



                    if( $order_nope===0) {



                    }else{
              ?>

              <hr class="mb-4">


              <h4 class="mb-3">Payments</h4>

              <!-- You can choose between Admin or Seller for to send the money -->


              <?php
              // Send money directly to seller
                if($checko['send']==0) {


              echo'<a class="btn btn-dark" href="../payment_request.php?id='.$id_order_now.'">Payment now to '.$username.' in <i class="fa fa-btc"></i>itcoin</a>';

                 ?>

              <!--<h6 class="text-center">OR</h6>-->


              <?php // Send money to Admin of website

              //echo'<a class="btn btn-dark" href="https://www.blockchain.com/btc/payment_request?address='.$admin_btc.'&message=Admin&amount_local='.$totalPrice1.'&currency=USD&nosavecurrency=true">Deposit money to the administrator</a>';

              ?>

              <!--<p class="alert alert-info">The payment will be more secured but send to Admin of Website not now to vendor but the Administrator will add 5% to the price before to send the sum of the seller.</p>-->

              <?php

                    }else{

                    echo'<button class="btn btn-dark" disabled>You have already send the payment.</button>';


                }

        }





              ?>
            <hr />
              <img style="margin-right:auto;margin-left:auto;display:block;" src="images/marketyBtc.png" height="70px" width="200px" />
          </ul>
        </div>
        <div class="col-md-5 order-md-1">
            <?php if($order_nope===0) { // if the customer to delete the order ?>

            <p class="card-text emptyCart my-5 text-center">You don't have order for moment !</p>

            <?php }else{ ?>
          <h4 class="mb-3">Seller info</h4>
          <form class="needs-validation" method="POST">
            <div class="row">
            </div>

            <div class="mb-3">
              <label for="username">Username</label>
              <div class="input-group">
                <input type="text" class="form-control" name="username" id="username" value="<?=((isset($username) )?$username:'')?>" disabled>
              </div>
            </div>

            <div class="mb-3">
              <label for="username">Email</label>
              <div class="input-group">
                <input type="text" class="form-control" name="username" id="username" value="<?=((isset($email) )?$email:'')?>" disabled>
              </div>
            </div>

            <div class="row">
            </div>

            <hr class="mb-4">

              <?php if(empty($checko['keys_code'])) { ?>
                <a href="../messages/message.php?user=<?=$username?>" class="btn btn-dark btn-block">Chat with the seller</a>
                <p class="alert alert-info">The status of your orders will be available <a href="mykeys.php">mykeys.php</a></p>
              <?php }else{ ?>
                <form method="POST">
                  <button type="submit" name="#" class="btn btn-dark btn-block">My key now</button>
                </form>
                  <p class="alert alert-info">Pressing this button for to have key. And go to 'my keys'.</p>
              <?php } ?>


            <?php } ?>
        </div>
      </div>
        <footer class="my-5 pt-5 text-muted text-center text-small">

        <ul class="list-inline">
        </ul>
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../includes/jquery/validate.js"></script>
    <script src="../includes/jquery/ajaxform.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>
  </body>
</html>
