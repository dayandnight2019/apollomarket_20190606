<?php session_start(); include('../includes/config.php');

    $source = "https://www.blockchain.com/ticker";
    $file = file_get_contents($source);
    $btc = json_decode($file, true);

    $user_id = htmlspecialchars(@$_SESSION['id']);
    $GLOBAL_Current_User_Info = $bdd->prepare("SELECT * FROM members WHERE id = ?");
    $GLOBAL_Current_User_Info->execute(array($user_id));

    $GLOBAL_Current_User_Info = $GLOBAL_Current_User_Info->fetch();

    if($GLOBAL_Current_User_Info['vendor']<= 1) { header('Location: ../index.php'); }

    $GLOBAL_Admin_User_Info = $bdd->query("SELECT * FROM adminuser");
    $GLOBAL_Admin_User_Info = $GLOBAL_Admin_User_Info->fetch();

    $locks = $bdd->prepare("SELECT * FROM locks WHERE user_lock = ?");
    $locks->execute(array($user_id));
    $locks_now = $locks->fetch();

    $totalPrice = 0;

    $priceLock = $locks_now['price'];

    $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$priceLock."";
    $file1 = file_get_contents($source2);
    $btc1 = json_decode($file1, true);
?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Markety</title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!-- Custom styles for this template -->
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js" integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWc+VRZkx5xU3daN+lYb49+Ax+Tl" crossorigin="anonymous"></script>

      <link href="../includes/css/shop-homepage.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="../index.php">Markety</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">

              <form action="../search.php" class="form-inline mt-2 mt-md-0" method="GET">
            <input name="q" class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search" required>
            <button style="margin-right:10px;" class="btn btn-outline-success my-2 my-sm-0" type="submit"><i class="fa fa-search"></i></button>
            </form>


              <?php if(!isset($_SESSION['id'])) { ?>
                    <?php if(!isset($_SESSION['member_id'])) { ?>
              <li class="nav-item active">
                <a style="margin-right:10px;" class="btn btn-success" href="becomeseller.php"><i class="fa fa-plus-square"></i> Become a seller</a>
              </li>
            <li class="nav-item active">
              <a class="nav-link" href="login.php">Log In</a>
            </li>
              <li class="nav-item active">
              <a class="nav-link" href="register.php">Register</a>
                    <?php }else{ ?>
                   <li class="nav-item active">
                <a class="nav-link" href="../admin/index.php">Admin Panel</a>
                </li>


                  <?php } ?>
            </li>
              <?php }else{ ?>

               <?php if($GLOBAL_Current_User_Info['vendor']==1 or $GLOBAL_Current_User_Info['vendor']==2 or $GLOBAL_Current_User_Info['vendor']==3) { ?>
            <li class="nav-item active">
                <a style="margin-right:10px" class="nav-link btn btn-success" href="../articles/addarticle.php"><i class="fa fa-plus-square"></i> Add Product</a>
              </li>
            <?php }else{ ?>

            <li class="nav-item active">
                <a style="margin-right:10px;" class="btn btn-success" href="../becomeseller.php"><i class="fa fa-plus-square"></i> Become a seller</a>
              </li>

            <?php } ?>

            <li class="nav-item active">
                <a style="margin-right:10px" class="nav-link btn btn-success" href="../ticket.php"><i class="fas fa-life-ring"></i> Support</a>
            </li>


                 <?php  if(empty($GLOBAL_Current_User_Info['ppicture'])) { ?>
              <li class="nav-item active">
                <img style="border-radius:200px;" src="images/default.png" height="35px" width="35px" />
            </li>
                <?php }else{ ?>

              <li class="nav-item active">
                <img style="border-radius:200px;" src="uploads/<?=$GLOBAL_Current_User_Info['ppicture']?>" height="35px" width="35px" />
            </li>
                <?php } ?>
            <li class="nav-item active">
                <a class="nav-link" href="member.php?username=<?=$_SESSION['username']?>&id=<?=$_SESSION['id']?>&pn=1"><?=$_SESSION['username']?></a>
            </li>
              <?php

                    $cart_number = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? and done='0'");
                    $cart_number->execute(array($user_id));
                    $cart_numbers = $cart_number->rowCount();

              ?>
              <li class="nav-item active">
                <a class="nav-link" href="cart.php"><i class="fa fa-shopping-cart"> (<?=$cart_numbers?>)</i></a>
                </li>
              <li class="nav-item active">
                <a class="nav-link" href="../messages/message.php"><i class="fa fa-envelope"> </i></a>
                </li>
             <li class="nav-item active">
                <a class="nav-link" href="logout.php"><i style="color:red;" class="fa fa-power-off"></i></a>
            </li>
              <?php } ?>
              <li class="nav-item active">
                  <a class="nav-link" ><i style="color:white;" class="btcHeader nav-link fa fa-btc"></i> : <?php  echo $btc["USD"]["last"]; ?> $</a>

          </ul>
        </div>
      </div>
    </nav>

<?php

$totalPrice += $btc1;

 ?>

<div class="container">
  <br />
  <h1 class="text-center"><span class="badge badge-danger"> <i class="fa fa-lock"></i> Account Locked <i class="fa fa-lock"></i></span></h1>
  <h1 class="text-center"><span class="badge badge-danger"> You did not pay the webmaster !</span></h1>
  <p class="lead text-center">You can pay the GLOBAL_Admin_User_Info of the website by this button. And you will be unlocked after receiving the desired funds!</p>
  <a class="btn btn-dark btn-block" href="https://blockchain.com/btc/payment_request?address=<?=$GLOBAL_Admin_User_Info['btc_address']?>&amount=<?=$totalPrice?>&message=<?=$GLOBAL_Admin_User_Info['username']?>"> Click here for to pay the GLOBAL_Admin_User_Info</a>
</div>
