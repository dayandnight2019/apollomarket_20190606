<?php include('header.php');

 if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
    }

$select = $bdd->prepare("SELECT * FROM members where id = ?");
$select->execute(array($user_id));

// Edit picture script with verification of format picture
	if($select->rowCount()==1){

		$result = $select->fetch();
		//echo'<pre>';print_r($result);die();
        $ppicture = $result['ppicture'];

        if(isset($_POST['editPicBtn'])) {

            $filename = $_FILES['ppicture']['name'];
        	  $tmp_name = $_FILES['ppicture']['tmp_name'];
        	  $filename = rand(9999,10000).date('Ydmhis').$filename;
            $uploadOk = 1;

            $imageFileType = strtolower(pathinfo($filename,PATHINFO_EXTENSION));

                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
                && $imageFileType != "gif" ) {

                    $errors = "<div class='alert alert-danger'>Sorry, only JPG, JPEG, PNG & GIF files are allowed.</div>";
                    $uploadOk = 0;

                    }else{

                    $uploadOk = 1;
        	          move_uploaded_file($tmp_name, 'uploads/' .$filename);

                    $update_pic = $bdd->prepare("UPDATE members SET ppicture = ? where id = ?");
                    $update_pic->execute(array($filename,$user_id));

                    $success_pic = "<div class='alert alert-success'>Success !</div>";

                         //header('Location: editpic.php');

                }


        }else{




        }


       }


?>

    <!-- Page Content -->
    <div class="container">

      <!-- Team Members Row -->
      <div class="row">

        </div>
		<br>
        <div style="float:left;" class="col-lg-4">
          <h1 class="my-4"></h1>
			<div class="list-group">
				<a href="editpic.php" class="list-group-item active"><i class="fas fa-image"></i> Edit Picture</a>
				<a href="editprofile.php" class="list-group-item"><i class="fas fa-info-circle"></i> Edit Infos</a>
				<a href="pgpkey.php" class="list-group-item"><i class="fas fa-key"></i> PGP Key</a>
				<?php
					if($result['vendor'] > 0 )
					{
						?>
							<a href="wallet.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet</a>						
						<?php
					}
					else
					{
						?>
						<a href="walletreturn.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet Return</a>						
						<?php
					}
				?>
				<a href="editpwd.php" class="list-group-item"><i class="fas fa-key"></i> Edit Password</a>
			</div>
        </div>
        <div class="col-lg-11 col-sm-12 text-center mb-1">
            <form style="float:right;margin-top:30px" class="editMember" method="POST" enctype="multipart/form-data">
                <?php echo @$errors; ?>
                <?php echo @$success_pic; ?>
                <!-- Picture -->
                <?php if(empty($ppicture)) { ?>
                    <img class="rounded-circle img-fluid d-block mx-auto" src="images/default.png" height="200px" width="200px" alt="">
                <?php }else{ ?>
                    <img class="rounded-circle img-fluid d-block mx-auto" src="uploads/<?=$ppicture?>" height="200px" width="200px" alt="">
                <?php } ?>
            <div class="form-group">
              <div class="form-label-group">
                <input type="file" accept="image/x-png, image/gif, image/jpeg" id="inputFile" class="form-control" name="ppicture" >
                </div>
            </div>
                <button class="btn btn-dark btn-block" name="editPicBtn"><i class="fas fa-wrench"></i> Update Picture</button>
            </form>
    </div>
</div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
