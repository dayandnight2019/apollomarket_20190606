<?php include('header.php');

if(!isset($_SESSION['id'])) {
  header('Location: ../index.php');
}

?>

<div class="container">
    <h2 class="text-center">My purchases stuff</h2>
<div class="card mb-5">
            <div class="card-header">
              <i class="fa fa-btc"></i> |
             My purchases stuff</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Order ID</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th>Contact</th>
                      <th>Date added</th>
                    </tr>
                  </thead>
                  <tbody>
             <?php
                // show feedback of user_id
                $article_order = $bdd->prepare("SELECT * FROM orders WHERE user_from = ?");
                $article_order->execute(array($user_id));
                      while($row = $article_order->fetch()) {

                          $id_art =    $row['id'];
                          $id_article = $row['article_id'];
                          $order_id = $row['order_article'];
                          $sent_or_not = $row['sent'];
                  // Show seller of feedback
                $arti_fee = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
                $arti_fee->execute(array($id_article));
                        while($row1 = $arti_fee->fetch()) {

                            $id_order = $row1['id'];
                            $customer_id = $row1['user_id'];
                            $name_article = $row1['title'];

                  // show cateogry of product who is order
                $show_cate = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $show_cate->execute(array($customer_id));
                        while($row2 = $show_cate->fetch()) {

                            $seller_feedback = $row2['username'];

                      echo'<tr>';
                      echo'<td>'.$id_art.'</td>';
                      echo'<td>'.$order_id.'</td>';
                      echo'<td>'.$name_article.'</td>';

                  if($sent_or_not==0) {
                      echo'<td><span class="badge badge-info">Waiting to send the product </span></td>';
                      echo '<td><a class="btn btn-dark btn-block" href="../messages/message.php?user='.$seller_feedback.'">Contact</a></td>';
                    }else{

                          if($sent_or_not==1) {
                            echo'<td><span class="badge badge-success">Product sent</span></td>';
                            echo '<td><a class="btn btn-dark btn-block" href="../messages/message.php?user='.$seller_feedback.'">Contact</a></td>';
                          }

                          if($sent_or_not==2) {
                            echo'<td><span class="badge badge-success">Orders Canceled</span></td>';
                            echo '<td><button class="btn btn-dark btn-block" disabled>Contact</button></td>';
                          }


                        }
                      echo'<td>'.$row['date_added'].'</td>';
                      echo'</tr>';


                            }
                          }
                        }

            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>

                  </div>


    <!-- Bootstrap core JavaScript-->
    <script src="../admin/vendor/jquery/jquery.min.js"></script>
    <script src="../admin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../admin/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="../admin/vendor/chart.js/Chart.min.js"></script>
    <script src="../admin/vendor/datatables/jquery.dataTables.js"></script>
    <script src="../admin/vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="../admin/js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="../admin/js/demo/datatables-demo.js"></script>
    <script src="../admin/js/demo/chart-area-demo.js"></script>
