<?php include('header.php'); include('../includes/pagination_member.php');

  $username = htmlspecialchars($_GET['username']);

	$userInfo = $bdd->prepare("SELECT * FROM members WHERE username = ?");
  $userInfo->execute(array($username));
	$rsuserInfo = $userInfo->fetch();

  $userSeller = $bdd->prepare("SELECT * FROM members WHERE username = ?");
  $userSeller->execute(array($username));
	$rsuserSeller = $userSeller->fetch();

  $feed_show = $bdd->prepare("SELECT * FROM members WHERE username = ?");
  $feed_show->execute(array($username));
  $feedback_show = $feed_show->fetch();

  $user_show = $feedback_show['id'];

  $show_feed = $bdd->prepare("SELECT * FROM articles WHERE user_id = ?");
  $show_feed->execute(array($user_show));
  $show_feedback = $show_feed->fetch();

  $article_id_show = $show_feedback['id'];

  $feed_good = $bdd->prepare("SELECT * FROM feedback WHERE for_id = ? AND good='a'");
  $feed_good->execute(array($user_show));
  $feedback_good = $feed_good->rowCount();

  $feed_good_no = $bdd->prepare("SELECT * FROM feedback WHERE for_id = ? AND good='b'");
  $feed_good_no->execute(array($user_show));
  $feedback_good_not = $feed_good_no->rowCount();




?>

<!-- Page Content -->
<div class="container">
    <?php echo @$success;
          echo @$delete_success;
    ?>
  <!-- Team Members Row -->
  <div class="row">
    <div class="col-lg-4">
        <br />

        <!-- Picture -->
        <?php if(empty($rsuserInfo['ppicture'])) { ?>
            <img class="rounded-circle img-fluid d-block mx-auto" src="images/default.png" height="200px" width="200px" alt="">
        <?php }else{ ?>
            <img style="border: 5px solid #343a40;" class="rounded-circle img-fluid d-block mx-auto" src="uploads/<?=$rsuserInfo['ppicture']?>" height="250px" width="250px" alt="">
        <?php } ?>
    </div>
    <div class="col-lg-8">
      <br>
      <div style="padding:20px;" class="card">
      <?php
        // show number of sales
        $sal = $bdd->prepare("SELECT * FROM members WHERE id = ?");
        $sal->execute(array($user_id));
        $sales = $sal->fetch();


        ?>

         <h3><?=$rsuserInfo['username']?>
        <!-- Button Edit -->
      <?php


                  // show Edit Produt / Order Pending and Number of sale
        ?>
         <?php if($rsuserInfo['vendor']==0) { // if sales of seller are == 0 ?>
            <small> | Customer </small>
        <?php }else{

                if($rsuserInfo['sales'] < 10) {  // if sales of seller are < 10

          ?>
            <small> | Beginner Seller ( <a href="myfeedback.php?username=<?=$username?>"><i style="color:#43f743;"><?=$feedback_good?></i> /  <i style="color:red;"><?=$feedback_good_not?></i></a> )</small>
          <?php
          }else{

                if($rsuserInfo['sales'] < 20) { // if sales of seller are < 20
               ?>
            <small> | Pro Seller ( <a href="myfeedback.php?username=<?=$username?>"><i style="color:#43f743;"><?=$feedback_good?></i> /  <i style="color:red;"><?=$feedback_good_not?></i></a> )</small>

              <?php
            }else{
                if($rsuserInfo['sales'] > 20) { // if sales of seller are > 20
                     ?>

            <small> | Boss Seller <a href="myfeedback.php?username=<?=$username?>"><i style="color:#007bff;" class="fa fa-check-circle"></i> ( <i style="color:#43f743;"><?=$feedback_good?></i> /  <i style="color:red;"><?=$feedback_good_not?></i></a> )</small>

                    <?php

                        }

                    }
                }

            }


         ?>
                </h3>
        <?php if((@$_SESSION['id'])===($rsuserInfo['id'])) { ?>

          <!--<a href="#" >Sales <span class="badge badge-dark"></span></a>-->
          <a href="tickets.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-ticket-alt"></i> My Tickets</span></a>
          <a href="mykeys.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-shopping-cart"></i> My Purchases</span></a>
          <a href="feedbacks.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-comment"></i> Feedback</span></a>
          <a href="pending.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-money-bill"></i> Order pending</span></a>
          <a href="editpic.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-edit"></i> Edit Profile</span></a>
          <a href="logout.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-sign-out-alt"></i> Logout</span></a>


        <?php  if($rsuserInfo['vendor'] < 1){ ?>

          <a href="tickets.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-ticket-alt"></i> My Tickets</span></a>
          <a href="mykeys.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-shopping-cart"></i> My Purchases</span></a>
          <a href="feedbacks.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-comment"></i> Feedback</span></a>
          <a href="editpic.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-edit"></i> Edit Profile</span></a>
          <a href="logout.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-sign-out-alt"></i> Logout</span></a>

              <?php } ?>

        <?php }else{ ?>
              <a href="../reports.php?user=<?=$username?>">Report Scam</a>
             <a href="../messages/message.php?user=<?=$username?>">Contact <?=$username?></a>
           <?php } ?>
      </div>
      </div>
      <div class="col-lg-12">
            <h3 style="margin-top:20px;" class="text-center">All feedbacks of <?=$rsuserInfo['username']?></h3>
            <hr />
        </div>
          </div>

          <div class="container">
            <!-- Project One -->
            <?php $my_feedback = $bdd->prepare("SELECT * FROM members WHERE username = ?");
                  $my_feedback->execute(array($username));
                  while($row = $my_feedback->fetch()) {
                          $id_user = $row['id'];

                  $article_feedback = $bdd->prepare("SELECT * FROM articles WHERE user_id = ?");
                  $article_feedback->execute(array($id_user));
                  while($row1 = $article_feedback->fetch()) {
                          $id_article = $row1['id'];

                  $article_show = $bdd->prepare("SELECT * FROM feedback WHERE article_id = ? ORDER BY id DESC");
                  $article_show->execute(array($id_article));
                  while($row2 = $article_show->fetch()) {
                          $id_article_feed = $row2['article_id'];
                          $text_feedback_show = $row2['text_feedback'];
                          $good_or_no = $row2['good'];

                  $article_feed = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
                  $article_feed->execute(array($id_article_feed));
                  while($row3 = $article_feed->fetch()) {
                          $title_article = $row3['title'];

                  echo'<div class="row feedCustomer">';
                  echo'<div class="col-md-1">';
            if($good_or_no == 'a') {
                  echo'<div><h3><i class="fa fa-thumbs-up goodFeed"></i></h3></div>';
                  $good_or_no=1;
                }else{
                  if($good_or_no == 'b') {
                  echo'<div><h3><i class="fa fa-thumbs-down goodNoFeed"></i></h3></div>';
                  $good_or_no=1;
                }
                }

                  echo'</div>';
                      echo'<div class="col-md-11">';
                        echo'<span>'.$text_feedback_show.'</span>';
                        echo'<p><a href="../articles/article.php?id='.$row3['id'].'"><small>'.$title_article.'</small></a></p>';
                      echo'</div>';
                    echo'</div>';
                    echo'<br>';



                              }
                          }
                      }
                  }

             ?>
          </div>

    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
