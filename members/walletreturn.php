<?php include('header.php');

 if(!isset($_SESSION['id'])) {
    header('Location: ../404.php');
    }

$select = $bdd->prepare("SELECT * FROM members where id = ?");
$select->execute(array($user_id));
if($select->rowCount()==1){
	$result = $select->fetch();
	
}

?>

<!-- Page Content -->
<div class="container">
	<!-- Team Members Row -->
	<div class="row"></div>
	<br>
	<div style="float:left;" class="col-lg-4">
		<h1 class="my-4"></h1>
			<div class="list-group">
				<a href="editpic.php" class="list-group-item"><i class="fas fa-image"></i> Edit Picture</a>
				<a href="editprofile.php" class="list-group-item"><i class="fas fa-info-circle"></i> Edit Infos</a>
				<a href="pgpkey.php" class="list-group-item"><i class="fas fa-key"></i> PGP Key</a>
				<?php
					if($result['vendor'] > 0 )
					{
						?>
							<a href="wallet.php" class="list-group-item"><i class="fas fa-wallet"></i> Wallet</a>						
						<?php
					}
					else
					{
						?>
						<a href="walletreturn.php" class="list-group-item active"><i class="fas fa-wallet"></i> Wallet Return</a>						
						<?php
					}
				?>
				<a href="editpwd.php" class="list-group-item"><i class="fas fa-key"></i> Edit Password</a>
			</div>
	</div>
</div>
<!-- /.container -->
<!-- Bootstrap core JavaScript -->
<script src="../includes/vendor/jquery/jquery.min.js"></script>
<script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
