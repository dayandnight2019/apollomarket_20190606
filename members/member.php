<?php include('header.php'); ; include('../config.php'); //include('../includes/pagination_member.php')



if(isset($_GET['id'])) {
	$userInfo = $bdd->prepare("SELECT * FROM members WHERE id=?");
	$userInfo->execute(array($_GET['id']));
	$rsuserInfo = $userInfo->fetch();
	$id = htmlspecialchars($_GET['id']);
} elseif(isset($_GET['username'])) {
	$userInfo = $bdd->prepare("SELECT * FROM members WHERE username='?'");
	$userInfo->execute(array($_GET['username']));
	$rsuserInfo = $userInfo->fetch();
	$id = $rsuserInfo['id'];
} else {
	die('Missing `id` or `username` GET parameter.');
}

$article =  htmlspecialchars(@$_GET['article']);

$produ = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$produ->execute(array($article));
$produc = $produ->fetch();



//$orderShow = $bdd->prepare("SELECT * FROM orders WHERE for_id = ?");
//$orderShow->execute(array($id));
//$orderShower = $orderShow->rowCount();

$orderShow1 = $bdd->prepare("SELECT * FROM orders WHERE for_id = ?");
$orderShow1->execute(array($id));
$orderShower1 = $orderShow1->fetch();

$articleVend = $bdd->prepare("SELECT SUM(price) AS prix_total FROM articles WHERE id = ?");
$articleVend->execute(array($orderShower1['article_id']));
$articleVendors  = $articleVend->fetch();

$feed_good = $bdd->prepare("SELECT * FROM feedback WHERE for_id = ? AND good='a' ");
$feed_good->execute(array($id));
$feedbackGood= $feed_good->rowCount();

$feed_not_good = $bdd->prepare("SELECT * FROM feedback WHERE for_id = ? AND good='b' ");
$feed_not_good->execute(array($id));
$feedbackNotGood = $feed_not_good->rowCount();



if(isset($_POST['deleteProduct'])) {
	$id_rdv = htmlspecialchars($_POST['id']);
	$delete_article = $bdd->prepare("DELETE FROM articles where id = ?");
	$delete_article->execute(array($id_rdv));
	$delete_success = '<div class="alert alert-success">Product deleted with success !</div>';
}

?>

    <!-- Page Content -->
	<br>
	<br>
    <div class="container">
        <?php echo @$delete_success; ?>
      <!-- Team Members Row -->
      <div class="row">
        <div class="col-lg-4">
            <br />

            <!-- Picture -->
            <?php if(empty($rsuserInfo['ppicture'])) { ?>
                <img class="rounded-circle img-fluid d-block mx-auto" src="images/default.png" height="200px" width="200px" alt="">
            <?php }else{ ?>
                <img style="border: 5px solid #343a40;" class="rounded-circle img-fluid d-block mx-auto" src="uploads/<?=$rsuserInfo['ppicture']?>" height="250px" width="250px" alt="">
            <?php } ?>
        </div>
        <div class="col-lg-8">
          <br>
          <div style="padding:20px;background-color:#f7f6f6" class="card">
          <?php
            // show number of sales
            $sal = $bdd->prepare("SELECT * FROM members WHERE id = ?");
            $sal->execute(array($user_id));
            $sales = $sal->fetch();

			
			
			
			//User name and title
			echo '<h3>'.$rsuserInfo['username'];
			if($rsuserInfo['vendor']==0) { 
				echo '<small> | Customer </small>';
            }else{
				if($rsuserInfo['sales'] < 10) { echo '<small> | Beginner Seller'; }
				elseif($rsuserInfo['sales'] < 20) { echo '<small> | Pro Seller'; }
				elseif($rsuserInfo['sales'] > 20) { echo '<small> | Boss Seller'; }
				echo '( <a href="feedback.php?id='.$username.'"><i style="color:#43f743;">'.$feedbackGood.'</i> /  <i style="color:red;">'.$feedbackNotGood.'</i></a> )</small>';
			}
            echo '</h3>';
            
			
			
			
			
			
			
			
			if((@$_SESSION['id'])===($rsuserInfo['id'])) {
				if($rsuserInfo['vendor'] >= 1){ 
					echo '<a href="feedbacks.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-comment"></i>My Feedback</span></a>';
				}
				echo '
				<a href="tickets.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-ticket-alt"></i> My Tickets</span></a>
				<a href="mykeys.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-shopping-cart"></i> My Purchases</span></a>
				<a href="pending.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-money-bill"></i> My Pending Orders</span></a>
				<a href="editpic.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-edit"></i> Edit Profile</span></a>
				<a href="../ticket.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-ticket-alt"></i> Report</span></a>
				<a href="logout.php"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-sign-out-alt"></i> Logout</span></a>
				';
			 }
			 
			if($rsuserInfo['vendor'] >= 1){ 
				echo '
				<a href="../reports.php?user='.$username.'"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-flag"></i> Report Scam</span></a>
				<a href="../messages/message.php?user='.$username.'"><span style="font-size: 17px;margin-bottom:5px;" class="badge badge-dark"><i class="fas fa-comments"></i> Contact '.$username.'</span></a>
				';
			}
			
			
			
			
			

				?>
          </div>
          </div>
          <div class="col-lg-12">
            <h3 style="margin-top:20px;" class="text-center"><?php echo $rsuserInfo['username']; ?>'s Products: </h3>
            <hr />
          </div>
          <?php

            $product = $bdd->prepare("SELECT * FROM articles WHERE user_id = ? ORDER BY id DESC");
            $product->execute(array($id));

            // Show all seller products
            while($row = $product->fetch()) {

                $id_article =  $row['id'];
                $tit_article = $row['title'];
                $descri_article = $row['description'];
                $pri_article = $row['price'];
                $pic_article = $row['ppicture'];

                // Blockchain show price in BTC
                $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$pri_article."";
                $file1 = file_get_contents($source2);
                $btc1 = json_decode($file1, true);

            echo'<div class="col-lg-3 col-md-6 mb-4">';
              echo'<div class="card h-100">';
              echo'<a href="../articles/article.php?id='.$id_article.'"><img class="card-img-top" src="../articles/uploads/'.$pic_article.'" alt=""  height="500px" width="500px" ></a>';
                echo'<div class="card-body">';
                  echo'<h6 class="card-title text-center">';
                   echo '<a style="text-decoration:none;color:black;" href="../articles/article.php?id='.$id_article.'">'.$tit_article.'</a>';
                  echo'</h6>';
                    echo '<h6 class="text-center">Shipping : '.$row['shipping_country'].'</h6>';
                  echo'<h6 class="text-center">'.$pri_article.' $ / '.$btc1.' <i class="fa fa-btc"></i></h6>';

                  if($user_id===$id) {

                echo '<a href="../articles/editarticle.php?id='.$id_article.'"  class="btn btn-dark btn-block">Edit Product</a>';
                echo'<hr />';
                echo'<form method="POST">';
                 echo'<input type="hidden" name="id" value="'.$id_article.'">';
                 echo '<button type="submit" name="deleteProduct"  class="btn btn-danger btn-block">Delete Product</button>';
               }
                 echo'</form>';
                echo'<h5></h5>';
                echo'</div>';
              echo'</div>';
            echo'</div>';

            }
        ?>
            <!--<p id="pagination_controls"><?php //echo $paginationCtrls; ?></p>-->
    </div>
  </div>
    <!-- /.container -->


    <!-- Bootstrap core JavaScript -->
    <script src="../includes/vendor/jquery/jquery.min.js"></script>
    <script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
