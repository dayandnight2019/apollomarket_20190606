<?php include('header.php');

if(!isset($_SESSION['id'])) {
  header('Location: index.php');
}

$username = $_SESSION['username'];

$user_ticket = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$user_ticket->execute(array($user_id));
$user_ticket_show = $user_ticket->fetch();

//$admin_ticket = $bdd->query("SELECT * FROM adminuser");
//$admin_tickets = $admin_ticket->fetch();
//$user_to = $admin_tickets['username'];
//$user_to = $GLOBAL_Admin_User_Info['username'];

if(isset($_REQUEST['addRepBtn'])) {
	$text_ticket = htmlspecialchars($_POST['catego']).' - '.htmlspecialchars($_POST['text_ticket']);
	$ticket_id = substr(md5(rand().microtime(true)), 0, 24);
	$insert_support = $bdd->prepare("INSERT INTO support (username,text_last,close, date_added) VALUES(?,?,'0',NOW())");
	$insert_support->execute(array($username,$text_ticket));
	echo 'Ticket created successfully.';
}

?>
<br>
<div class="container">

    <div class="col-lg-10 ">
      <br>
        <h3 class="text-center"><i class="fas fa-ticket-alt"></i> | Ticket Support</h3>
        <hr>
    <form method="POST">


                <label for="inputUsername">Username</label>
                <input style="background-color:#f1f1f1;cursor:not-allowed;" type="text" name="username" class="form-control-members" value="<?=$user_ticket_show['username']?>" disabled>
                <label for="inputUsername">Reason</label>
                    <select name="catego" class="form-control">
                        <option>Scammer</option>
                        <option>Fed</option>
                        <option>Fake profile</option>
                        <option>Botting Reviews</option>
                        <option>Concern Or Comment</option>
                        <option>Idea or Questions</option>
                        <option>Job Inquiry</option>
                    </select>
                <label for="inputUsername">Description</label>
                <textarea style="min-height:300px;" id="editor1" class="form-control-members" name="text_ticket" ></textarea>
                <script>
        CKEDITOR.replace( 'editor1' );
    </script>
          <?php
          $username = $_SESSION['username'];
          $tickerNumber = $bdd->prepare("SELECT * FROM support WHERE username = ?");
          $tickerNumber->execute(array($username));
          $ticketNumbers = $tickerNumber->rowCount();

          if($ticketNumbers >= 1) { ?>
            <input style="margin-top: 20px;" class="btn btn-dark btn-block" value="Send ticket" disabled>
        <?php }else{ ?>
            <input style="margin-top: 20px;" type="submit" name="addRepBtn" class="btn btn-dark btn-block" value="Send ticket" >
          <?php } ?>
          </form>
    </div>

</div>
<br>
