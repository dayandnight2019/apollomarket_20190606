<?php
include('header.php');
include('../includes/pagination_category.php');

$cat = mysqli_query($connection, "SELECT * FROM categories");

$cate = mysqli_real_escape_string($connection, @$_REQUEST['cat']);

$arti = mysqli_real_escape_string($connection, @$_REQUEST['art']);

$categ = mysqli_query($connection, "SELECT * FROM articles WHERE category='$cate'");
$categor = mysqli_fetch_array($categ);

$catego = mysqli_query($connection, "SELECT * FROM categories WHERE id='$cate'");
$cat_show = mysqli_fetch_array($catego);

// Delete product Button
if (isset($_REQUEST['deleteProduct'])) {

	$id_rdv = mysqli_real_escape_string($connection, $_POST['id']);

	$delete_article = "DELETE FROM articles where id='$id_rdv'";

	mysqli_query($connection, $delete_article);

	$delete_success = '<div class="alert alert-success">Product delete with success !</div>';
}


// Add to Cart Button
if (isset($_REQUEST['addCartBtn'])) {

	$order_article = rand(999, 50000);

	$insert_cart = "INSERT INTO orders SET
                    id='',
                    user_from='$user_id',
                    article_id='$arti',
                    order_article='$order_article',
                    date_added=NOW()
                    ";

	mysqli_query($connection, $insert_cart);

	$success = '<div class="alert alert-success">Product added to shopping cart !</div>';
} else { }

?>

<!-- Page Content -->
<div class="container">
	<?php echo @$success; ?>
	<div class="row">

		<div class="col-lg-3">
			<h6 class="my-4">Categories</h6>
			<div class="list-group">
				<?php while ($row = mysqli_fetch_assoc($cat)) {
					$id_ca =  $row['id'];
					$catego = $row['category'];

					echo '<a href="../category/category.php?cat=' . $id_ca . '&pn=1" class="list-group-item">' . $catego . '</a>';
				} ?>
			</div>

			<h6 class="my-4">Top seller</h6>
			<?php $seller_top = mysqli_query($connection, "SELECT * FROM members WHERE vendor='1' ORDER BY sales DESC LIMIT 0,5");
			while ($row1 = mysqli_fetch_array($seller_top)) {
				$seller_name = $row1['username'];
				$seller_pic = $row1['ppicture'];
				$sales_total = $row1['sales'];
				if ($sales_total >= 50) {
					echo '<div class="col-lg-4 col-sm-6 text-center mb-4">';
					echo '<img  class="rounded-circle img-fluid d-block mx-auto" src="../members/uploads/' . $seller_pic . '" width="150px" height="150px" alt="">';
					echo '<h6><a>' . $seller_name . '</a></h6>';
					echo '</div>';
				} else if ($sales_total >= 25) {
					echo '<div class="col-lg-4 col-sm-6 text-center mb-4">';
					echo '<img  class="rounded-circle img-fluid d-block mx-auto" src="../members/uploads/' . $seller_pic . '" width="150px" height="150px" alt="">';
					echo '<h6>' . $seller_name . '</h6>';
					echo '</div>';
				} else if ($sales_total >= 10) {
					echo '<div class="col-lg-4 col-sm-6 text-center mb-4">';
					echo '<img class="rounded-circle img-fluid d-block mx-auto" src="../members/uploads/' . $seller_pic . '" width="150px" height="150px" alt="">';
					echo '<h6>' . $seller_name . '</h6>';
					echo '</div>';
				}
			}

			?>

		</div>
		<!-- /.col-lg-3 -->

		<div class="col-lg-9">
			<br />
			<h3 class="text-center">Category : <?= $cat_show['category'] ?> </h3>
			<div class="row">
				<?php

				$product = mysqli_query($connection, "SELECT * FROM articles WHERE category='$cate' $limit ");

				// show article only with category choose
				while ($row = mysqli_fetch_assoc($product)) {

					$id_article =   $row['id'];
					$user_article = $row['user_id'];
					$tit_article = $row['title'];
					$descri_article = $row['description'];
					$pri_article = $row['price'];

					$source2 = "https://blockchain.info/tobtc?currency=USD&value=" . $pri_article . "";
					$file1 = file_get_contents($source2);
					$btc1 = json_decode($file1, true);

					$vend = mysqli_query($connection, "SELECT * FROM members WHERE id='$user_article'");

					while ($row1 = mysqli_fetch_assoc($vend)) {
						$vend_id = $row1['id'];
						$vend_user = $row1['username'];
						$vend_pic = $row1['ppicture'];

						echo '<div style="margin-top:20px;" class="col-lg-4 col-md-6 mb-4">';
						echo '<div class="card h-100">';
						echo '<a><i style="width:100%;text-align:center;color:white;height:110px;font-size:50px;line-height:110px;" class="fas fa-code-branch bg bg-dark"></i></a>';
						echo '<div class="card-body">';
						echo '<h6 class="card-title text-center">';
						echo '<a href="../articles/article.php?id=' . $id_article . '">' . $tit_article . '</a>';
						echo '</h6>';
						echo '<h6 class="text-center">' . $pri_article . ' $ / ' . $btc1 . ' <i class="fa fa-btc"></i></h6>';
						if (empty($vend_pic)) {
							echo '<h6 class="text-center"> Vendor : <img style="border-radius:200px;" src="../members/images/default.png" height="25px" width="25px"/> <a href="../members/member.php?username=' . $vend_user . '&id=' . $vend_id . '&pn=1">' . $vend_user . '</a></p>';
						} else {
							echo '<h6 class="card-text text-center">Vendor : <img style="border-radius:200px;" src="../members/uploads/' . $vend_pic . '" height="25px" width="25px" /> <a href="../members/member.php?username=' . $vend_user . '&id=' . $vend_id . '&pn=1">' . $vend_user . '</a></p>';
						}
						echo '</div>';

						echo '</div>';
						echo '</div>';
					}
				}
				?>


			</div>
			<!-- /.row -->
			<!-- PAGINATION HERE for the script here pagination_category -->
			<p id="pagination_controls"><?php echo $paginationCtrls; ?></p>
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->


<!-- Bootstrap core JavaScript -->
<script src="includes/vendor/jquery/jquery.min.js"></script>
<script src="includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>