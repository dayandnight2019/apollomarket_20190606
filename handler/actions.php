<?php

session_start();
include('../includes/config.php');


// Send message request
if ($_REQUEST['action'] == 'sendMessage') {

	$inboxUsers = mysqli_query($connection, "SELECT * FROM inbox_user WHERE
									(user1='" . $_SESSION['id'] . "' and user2='" . $_REQUEST['user_id'] . "')
									OR
									(user2='" . $_SESSION['id'] . "' and user1='" . $_REQUEST['user_id'] . "')
									");


	if (mysqli_num_rows($inboxUsers) == 0) {

		$insertInbox = mysqli_query($connection, "INSERT INTO inbox_user SET id='',
											user1='" . $_SESSION['id'] . "',
											user2='" . $_REQUEST['user_id'] . "',
											date_added=NOW()
											");
	}

	$user_id = mysqli_real_escape_string($connection, $_REQUEST['user_id']);

	$message = mysqli_query($connection, "INSERT INTO messages SET id='',
									sendingfrom='" . $_SESSION['id'] . "',
									sendingto='" . $_REQUEST['user_id'] . "',
									message='" . $_REQUEST["message_$user_id"] . "',
									date_added=NOW()
									");

	echo 'success';

	// Get message request
} else if ($_REQUEST['action'] == 'GetMessages') {

	$messages = mysqli_query($connection, "SELECT * FROM messages WHERE
								(sendingfrom='" . $_SESSION['id'] . "' and sendingto='" . $_REQUEST['user_id'] . "')

								OR

								(sendingto='" . $_SESSION['id'] . "' and sendingfrom='" . $_REQUEST['user_id'] . "')
	");

	$messageList = '';
	while ($message = mysqli_fetch_array($messages)) {

		$userInfo = mysqli_query($connection, "SELECT * FROM members where id='" . $message['sendingfrom'] . "'");
		$rsuserInfo = mysqli_fetch_array($userInfo);



		$profile = mysqli_query($connection, "SELECT * FROM members where id='" . $message['sendingfrom'] . "'");
		$rsprofile = mysqli_fetch_array($profile);


		if (isset($rsprofile['ppicture']) and $rsprofile['ppicture'] != '') {

			$img = '<a href="../members/@' . $rsprofile['username'] . '-' . $rsprofile['id'] . '-1.html"><img class="chatImg" src="../members/uploads/' . $rsprofile['ppicture'] . '" height="30px" width="30px" /></a>';
		} else {

			$img = '<a href="../members/@' . $rsprofile['username'] . '-' . $rsprofile['id'] . '-1.html"><img class="chatImg" src="../members/images/default.png" height="42px" width="42px" /></a>';
		}

		// Message Template HTML/CSS

		$messageList .= '		<div class="' . (($_SESSION['id'] == $message['sendingfrom']) ? 'left' : 'right') . ' incoming_msg">
                                  <div class=" ' . (($_SESSION['id'] == $message['sendingfrom']) ? 'avaleft' : 'avaright') . ' incoming_msg_img"> ' . $img . ' </div>
                                  <div class="received_msg">
                                    <div class="' . (($_SESSION['id'] == $message['sendingfrom']) ? 'tleft' : 'tright') . ' received_withd_msg">
                                      <p class="' . (($_SESSION['id'] == $message['sendingfrom']) ? 'mleft' : 'mright') . '">' . $message['message'] . '</p>
                                      </div>
                                  </div>
                                </div>
                                <br />
                                ';
	}

	echo $messageList;
	exit;

	// Get inbox users request
} else if ($_REQUEST['action'] == 'GetInboxUsers') {

	$inboxUsers = mysqli_query($connection, "SELECT * FROM inbox_user WHERE

									user1='" . $_SESSION['id'] . "'
									or
									user2='" . $_SESSION['id'] . "'
	");

	$inboxUsers1 = '';

	while ($result = mysqli_fetch_array($inboxUsers)) {

		if ($result['user1'] != $_SESSION['id']) {

			$userName = $result['user1'];
		}

		if ($result['user2'] != $_SESSION['id']) {

			$userName = $result['user2'];
		}

		$show_message_inb = mysqli_query($connection, "SELECT * FROM messages WHERE sendingfrom='" . $userName . "' ORDER BY id DESC");
		$show_message_inbox = mysqli_fetch_array($show_message_inb);

		$profile = mysqli_query($connection, "SELECT * FROM members where id='" . $userName . "'");
		$rsprofile = mysqli_fetch_array($profile);

		$userInfo = mysqli_query($connection, "SELECT * FROM members where id='" . $userName . "'");
		$rsuserInfo = mysqli_fetch_array($userInfo);


		if (isset($rsprofile['ppicture']) and $rsprofile['ppicture'] != '') {

			$img = '<img style="border-radius:200px;" src="../members/uploads/' . $rsprofile['ppicture'] . '" height="42px" width="42px" />';
		} else {

			$img = '<img style="border-radius:200px;" src="../members/images/default.png" height="42px" width="42px" />';
		}

		// Inbox user template HTML/CSS
		$inboxUsers1 .= '
							<div style="background-color:#e8e8e8;padding:10px;border-radius:10px;height:70px">
								<div class="chat-list">
                                        <div class="chat_img">
                                            ' . $img . '
                                        </div>
									</div>
								<div  class="notification-event">
									<a href="message.php?user=' . $rsuserInfo['username'] . '" class="h6 notification-friend">' . $rsuserInfo['username'] . '</a>
									<br />
									<span class="chat-message-item">-</span>
								</div>
							</div>
								<br />
							';
	}

	echo $inboxUsers1;
	exit;
}
