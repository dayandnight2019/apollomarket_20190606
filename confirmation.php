<?php include('header.php');

$order_article = htmlspecialchars($_GET['order_article']);

$token = htmlspecialchars($_GET['token']);

if(!isset($_SESSION['id'])) {
  header('Location: ../index.php');
}

if(empty($token)) {
	header('Location: 404.php');
}

if(!isset($token)) {
	header('Location: 404.php');
}

if(empty($order_article)) {
	header('Location: 404.php');
}

if(!isset($order_article)) {
	header('Location: 404.php');
}

$vens = $bdd->prepare("SELECT * FROM orders WHERE user_from= ? AND done='0'");
$vens->execute(array($user_id));
$vends = $vens->fetch();

$vends_article_id = $vends['article_id'];

$vendor1 = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$vendor1->execute(array($vends_article_id));
$vendors1 = $vendor1->fetch();


$ven = $bdd->prepare("SELECT * FROM orders WHERE order_article = ? AND token = ? AND done='0'");
$ven->execute(array($order_article,$token));
$vend = $ven->fetch();

$produc = $vend['article_id'];

$vendor = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
$vendor->execute(array($produc));
$vendors = $vendor->fetch();

$id_article = $vendors['id'];
$user_article = $vendors['user_id'];

$sellerSale = $bdd->prepare("SELECT * FROM members WHERE id = ?");
$sellerSale->execute(array($user_article));
$sellerSales = $sellerSale->fetch();


$earning = $vendors1['price'] + $sellerSales['earnings'];


$salesNumber = $sellerSales['sales'];


$salesNumberMore = $salesNumber + 1;


if(isset($_POST['confirmOrderBtn'])) {

$salesSeller = $bdd->prepare("UPDATE members SET sales = ?, earnings = ? where id = ?");
$salesSeller->execute(array($salesNumberMore,$earning,$user_article));


$insert_feedback = $bdd->prepare("INSERT INTO feedback (user_id,article_id,for_id,good,text_feedback,date_added) VALUES(?,?,?,'0','',NOW())");
$insert_feedback->execute(array($user_id,$id_article,$user_article));


$insert_orders = $bdd->prepare("UPDATE orders SET send='1', done='1' where user_from= ?");
$insert_orders->execute(array($user_id));


header('Location: members/mykeys.php');

}
?>



<div class="container">
	<h1 class="text-center">Your order #<?=$vend['order_article']?> is paid</h1>
		<form method="POST">
			<button type="submit" name="confirmOrderBtn" class="btn btn-success btn-block"><i class="fas fa-mouse-pointer"></i> Click here for confirm your purchase</button>
		</form>
	</div>
