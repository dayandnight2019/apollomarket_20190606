<?php
error_reporting(0);
session_start();
require("includes/config.php");
require("config.php");
require("classes/block_io.php");
require("classes/gateway.php");

$user_id = $_SESSION['id'];

$user_receiv = $bdd->prepare("SELECT * FROM orders WHERE user_from = ? AND done='0'");
$user_receiv->execute(array($user_id));
$user_receive = $user_receiv->fetch();

$id_ord =  $user_receive['id'];

$token_show = $articles_shows['token'];
$order_article = $articles_shows['order_article'];

$apiKey = $gateway['bitcoin_api_key'];
$pin = $gateway['secret'];
$version = 2; // the API version
$block_io = new BlockIo($apiKey, $pin, $version);
$received = $block_io->get_transactions(array('type' => 'received', 'addresses' => $gateway['bitcoin_address']));
$payment_status="0";
	if($received->status == "success") {
			$data = $received->data->txs;
			$dt = StdClass2array($data);
			foreach($dt as $k=>$v) {
				$txid = $v['txid'];
				$time = $v['time'];
				$amounts = $v['amounts_received'];
				$amounts = StdClass2array($amounts);
				foreach($amounts as $a => $b) {
					$recipient = $b['recipient'];
					$amount = $b['amount'];
				}
				$senders = $v['senders'];
				$senders = StdClass2array($senders);
				foreach($senders as $c => $d) {
					$sender = $d;
				}
				$confirmations = $v['confirmations'];
				if($time+600 > time()) {
					if($gateway['bitcoin_confirmations'] > $confirmations) {
						if($amount == $_SESSION['btc_amount']) {
							$payment_status = "completed";
						}
					}
				}
	}
}

if($payment_status == "completed") {

	echo '<span class="text text-success"><i class="fa fa-check"></i> Payment was received! For confirm your order click <a href="confirmation.php?order_article='.$order_article.'&token='.$token_show.'">here</a> !</span>';

} else {
	echo '<span class="text text-info"><i class="fa fa-spin fa-spinner"></i> Awaiting payment...';
}
?>
