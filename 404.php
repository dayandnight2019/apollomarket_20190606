<?php include('header.php'); ?>
<!-- 404 page redirection -->

<div class="container">
    <div class="text-center">
        <br />
        <br />
            <h2>Error <span class="badge badge-dark">404</span></h2>
            <p class="alert alert-info">You lost yourself ?</p>
        <a href="index.php"><button class="btn btn-dark">Return to index</button></a>
      </div>
</div>
