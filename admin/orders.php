<?php include('header.php');



// Delete Order request

if(isset($_POST['supBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = $bdd->prepare("DELETE FROM orders WHERE id = ? ");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'orders.php'</script>";

    $delete = "<div class='alert alert-success'>Order deleted !</div>";

}

if(isset($_POST['sellerPaidBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = mysqli_query($connection, "UPDATE orders SET send='3' WHERE id=?");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'orders.php'</script>";

    $delete = "<div class='alert alert-success'>Order deleted !</div>";

}



?>
<div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item " href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">


          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Orders</a>
            </li>
            <li class="breadcrumb-item active">Settings Orders</li>
          </ol>

            <!-- ORDERS TABLE -->

         <div class="card mb-3">
             <?php echo @$insert_send; ?>
             <?php echo @$insert_order; ?>
            <div class="card-header">
              <i class="fa fa-btc"></i> |
              Customer from Markety</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID Order</th>
                      <th>Customer</th>
                      <th>Article</th>
                      <th>Price</th>
                      <th>Btc Send</th>
                      <th>Status</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
             <?php
                $totalPrice = 0;
                $order = $bdd->query("SELECT * FROM orders" );

                      while($row = $order->fetch()) {

                          $id_ord =    $row['id'];
                          $order_article = $row['order_article'];
                          $user_from = $row['user_from'];
                          $article_id = $row['article_id'];
                          $done_order = $row['done'];
                          $send_order = $row['send'];

                $user_order = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $user_order->execute(array($user_from));

                      while($row1 = $user_order->fetch()) {
                          $customer_user = $row1['username'];

                $article_order = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
                $article_order->execute(array($article_id));

                      while($row2 = $article_order->fetch()) {
                          $article_seller = $row2['user_id'];
                          $article_name = $row2['title'];
                          $article_price = $row2['price'];

                $seller_order = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $seller_order->execute(array($article_seller));

                      while($row3 = $seller_order->fetch()) {
                              $seller_user = $row3['username'];
                              $seller_btc = $row3['btc_address'];

                          $source2 = "https://blockchain.info/tobtc?currency=USD&value=".$article_price."";
                          $file1 = file_get_contents($source2);
                          $btc1 = json_decode($file1, true);

                      echo'<tr>';
                      echo'<td>'.$order_article.'</td>';
                      echo'<td>'.$customer_user.'</td>';
                      echo'<td>'.$article_name.'</td>';
                      echo'<td>'.$btc1.' <i class="fa fa-btc"></i></td>';



                          if($send_order==0) {


                      echo'<td><span class="badge badge-info">Wait Payment</span></td>';

                        }else{

                      echo'<td><span class="badge badge-success"><i class="fa fa-btc"></i> send to admin !</span></td>';



                        }

                          if($done_order==0) {

                      echo'<td><span class="badge badge-info">Pending order</span></td>';

                          }else{

                                if($done_order==2){

                                  if($send_order==1) {
                      echo'<td>
                      <span class="badge badge-info">Go to vendors.php for to pay seller</span>
                      </td>';

                    }else{

                      echo'<td><span class="badge badge-success "><i class="fa fa-btc"></i> send with success !</button></td>';
                    }

                              }
                          }

                            ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_ord ?>">

                    <?php

                      echo'<td><button name="supBtn" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button></td>';
                    ?>
                    </form>

                    <?php
                      echo'</tr>';

                            }
                        }

                      }
                    }
            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->


      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-dark" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
