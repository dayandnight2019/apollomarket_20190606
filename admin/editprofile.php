<?php include('header.php');

if(!isset($_SESSION['member_id'])) {
    header('Location: login.php');
}

$user_id = $_SESSION['member_id'];

$select = $bdd->prepare("SELECT * FROM adminuser where id = ?");
$select->execute(array($user_id));
// show all data of prodile admin
	if($select->rowCount()==1){

		$result = $select->fetch();

		$username = $result['username'];
		$email = $result['email'];
    $btc_address = $result['btc_address'];
    $api_key = $result['api_key'];
    $secret_pin = $result['secret_pin'];
    $nameWebsite = $result['name_website'];

        if(isset($_POST['proBtn'])) {


            if( !empty($_POST['username']) and !empty($_POST['email']) and !empty($_POST['btc_address']) and !empty($_POST['api_key']) and !empty($_POST['secret_pin']) and !empty($_POST['nameWebsite']) ) {

            $username = htmlspecialchars($_POST['username']);
            $email = htmlspecialchars($_POST['email']);
            $btc_address = htmlspecialchars($_POST['btc_address']);
            $api_key = htmlspecialchars($_POST['api_key']);
            $secret_pin = htmlspecialchars($_POST['secret_pin']);
            $nameWebsite = htmlspecialchars($_POST['nameWebsite']);
            //$commission = htmlspecialchars($_POST['commission']);



            $update = $bdd->prepare("UPDATE adminuser SET username = ?,email = ?,btc_address = ?,api_key = ?,secret_pin = ?,name_website = ? where id = ?");
            $update->execute(array($username,$email,$btc_address,$api_key,$secret_pin,$nameWebsite,$user_id));

            $success = "<div class='alert alert-success'>Updated Info successfully !</div>";

            }else{



        	}

      }


       }



?>
    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item active" href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Edit Profile</a>
            </li>
            <li class="breadcrumb-item active">Edit Infos</li>
          </ol>

            <div class="container col-lg-6 col-sm-4 ">
            <form method="POST">
              <?php echo @$errors; ?>
              <?php echo @$success; ?>
            <div class="form-group">
                <label>Email</label>
                    <input type="email"  class="form-control" name="email" placeholder="Email address" value="<?=((isset($email) )?$email:'')?>" required="required" autofocus="autofocus">
            </div>
            <div class="form-group">
                <label>Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username" value="<?=((isset($username) )?$username:'')?>" required="required">
            </div>
            <div class="form-group">
                <label >Block.io BTC Address</label>
                    <input type="text" class="form-control" name="btc_address" placeholder="Username" value="<?=((isset($btc_address) )?$btc_address:'')?>" required="required">
            </div>

            <div class="form-group">
                <label >Block.io Api Key Bitcoin</label>
                    <input type="password" class="form-control" name="api_key" placeholder="your api_key bitcoin" value="<?=((isset($api_key) )?$api_key:'')?>" required="required">
            </div>
            <div class="form-group">
                <label >Block.io Secret PIN</label>
                    <input type="password" class="form-control" name="secret_pin" placeholder="your secret pin" value="<?=((isset($secret_pin) )?$secret_pin:'')?>" required="required">
            </div>
            <div class="form-group">
                <label >Name Website</label>
                    <input type="text" class="form-control" name="nameWebsite" placeholder="Name Website" value="<?=((isset($nameWebsite) )?$nameWebsite:'')?>" required="required">
            </div>
            <input type="submit" name="proBtn" class="btn btn-dark btn-block" value="Update infos" >
          </form>
            </div>
        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->


      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-dark" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
