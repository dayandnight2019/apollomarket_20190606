<?php include('header.php');

// Delete product request
if (isset($_POST['supBtn'])) {

  $id_rdv = htmlspecialchars($_POST['id']);

  $delete_query = $bdd->prepare("DELETE FROM articles WHERE id = ?");
  $delete_query->execute(array($id_rdv));

  echo "<script>window.location 'articles.php'</script>";

  $delete = "<div class='alert alert-success'>Product delete !</div>";
}


?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item ">
      <a class="nav-link" href="index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item dropdown ">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-user"></i>
        <span>Edit Profile</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="editpic.php">Edit Picture</a>
        <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
        <a class="dropdown-item" href="editpwd.php">Edit Password</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="vendor.php">View Vendor</a>
        <a class="dropdown-item" href="customer.php">View Customer</a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="reports.php">
        <i class="fa fa-thumbs-down"></i>
        <span>Report Scam</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="orders.php">
        <i class="fa fa-btc"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="becomeaseller.php">
        <i class="fas fa-cart-plus"></i>
        <span>Become a seller</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="privacy.php">
        <i class="  fa fa-balance-scale"></i>
        <span>Terms and condition</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="tickets.php">
        <i class="fa fa-life-ring"></i>
        <span>Support</span></a>
    </li>
    <li class="nav-item active">
      <a class="nav-link" href="articles.php">
        <i class="fa fa-cart-arrow-down"></i>
        <span>Articles</span></a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Category</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
        <a class="dropdown-item" href="categories/category.php">View Categories</a>
      </div>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Articles</a>
        </li>
        <li class="breadcrumb-item active">Products Settings</li>
      </ol>

      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-btc"></i> |
          Products from Markety</div>
        <div class="card-body">


          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Id</th>
                  <th>Vendor</th>
                  <th>Category</th>
                  <th>Title</th>
                  <th>Price</th>
                  <th>Delete</th>
                  <th>View</th>
                  <th>Date</th>
                </tr>
              </thead>
              <tbody>
                <?php

                $vendor = $bdd->query("SELECT * FROM articles ");

                while ($row = $vendor->fetch()) {

                  $id_art =    $row['id'];
                  $id_user_art = $row['user_id'];
                  $title_art = $row['title'];
                  $id_cate_art = $row['category'];
                  $price_art = $row['price'];
                  $date_art = $row['date_added'];

                  $show_vendor = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                  $show_vendor->execute(array($id_user_art));
                  while ($row1 = $show_vendor->fetch()) {

                    $vendor_art = $row1['username'];

                    $show_cate = $bdd->prepare("SELECT * FROM categories WHERE id = ?");
                    $show_cate->execute(array($id_cate_art));
                    while ($row2 = $show_cate->fetch()) {

                      $cate_art = $row2['category'];

                      echo '<tr>';
                      echo '<td>' . $id_art . '</td>';
                      echo '<td>' . $vendor_art . '</td>';
                      echo '<td>' . $cate_art . '</td>';
                      echo '<td>' . $title_art . '</td>';
                      echo '<td>' . $price_art . ' $</td>';

                      ?>
                      <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_art ?>">

                        <?php

                        echo '<td><button name="supBtn" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button></td>';
                        ?>

                      </form>

                      <?php
                      echo '<td><a href="../articles/article.php?id=' . $id_art . '"><button class="btn btn-dark"><i class="fas fa-eye"></i> View</button></a></td>';
                      echo '<td>' . $date_art . '</td>';
                      echo '</tr>';
                    }
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->

    <!-- Sticky Footer -->


  </div>
  <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-dark" href="logout.php">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>

</body>

</html>