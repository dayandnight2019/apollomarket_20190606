<?php include('header.php');

// Delete category script request
if (isset($_POST['deleteBtn'])) {

  $id_rdv = htmlspecialchars($_POST['id']);

  $insert_query = $bdd->prepare("DELETE FROM categories WHERE id = ? ");
  $insert_query->execute(array($id_rdv));

  echo "<script>window.location 'category.php'</script>";

  $delete = "<div class='alert alert-success'>The status sent was successfully updated</div>";
}


?>
<div id="wrapper">

  <!-- Sidebar -->
  <ul class="sidebar navbar-nav">
    <li class="nav-item ">
      <a class="nav-link" href="../index.php">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-user"></i>
        <span>Edit Profile</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="../editpic.php">Edit Picture</a>
        <a class="dropdown-item" href="../editprofile.php">Edit Infos</a>
        <a class="dropdown-item" href="../editpwd.php">Edit Password</a>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-users"></i>
        <span>Users</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item" href="../vendor.php">View Vendor</a>
        <a class="dropdown-item" href="../customer.php">View Customer</a>
      </div>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../reports.php">
        <i class="fa fa-thumbs-down"></i>
        <span>Report Scam</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../orders.php">
        <i class="fa fa-btc"></i>
        <span>Orders</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../becomeaseller.php">
        <i class="fas fa-cart-plus"></i>
        <span>Become a seller</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../tickets.php">
        <i class="fa fa-life-ring"></i>
        <span>Support</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="../articles.php">
        <i class="fa fa-cart-arrow-down"></i>
        <span>Articles</span></a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="category.php" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Category</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <a class="dropdown-item active" href="addcategory.php">Add Category</a>
        <a class="dropdown-item" href="category.php">View Categories</a>
      </div>
    </li>
  </ul>

  <div id="content-wrapper">

    <div class="container-fluid">

      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#">Category</a>
        </li>
        <li class="breadcrumb-item active">View Categories</li>
      </ol>

      <div class="card mb-3">
        <?php echo @$insert_send; ?>
        <?php echo @$insert_order; ?>
        <div class="card-header">
          Category from Markety</div>
        <div class="card-body">


          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Category</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $order = $bdd->query("SELECT * FROM categories");

                while ($row = $order->fetch()) {

                  $id_cat =    $row['id'];
                  $cate_name = $row['category'];

                  echo '<tr>';
                  echo '<td>' . $id_cat . '</td>';
                  echo '<td>' . $cate_name . '</td>';
                  echo '<td><a class="btn btn-dark" href="editcategory.php?id=' . $id_cat . '">Edit Category</a></td>';

                  ?>
                  <form method="POST">
                    <input type="hidden" name="id" value="<?php echo $id_cat ?>">

                    <?php

                    echo '<td><button type="submit" name="deleteBtn" class="btn btn-danger">Delete</button></td>';
                    ?>
                  </form>

                  <?php


                  echo '</tr>';
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->

    <!-- Sticky Footer -->


  </div>
  <!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-dark" href="logout.php">Logout</a>
      </div>
    </div>
  </div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="../vendor/chart.js/Chart.min.js"></script>
<script src="../vendor/datatables/jquery.dataTables.js"></script>
<script src="../vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="../js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="../js/demo/datatables-demo.js"></script>
<script src="../js/demo/chart-area-demo.js"></script>

</body>

</html>