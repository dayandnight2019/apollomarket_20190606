<?php include('header.php');

$id =  htmlspecialchars($_GET['id']);

// if (!isset($_SESSION['member_id'])) {
// 	header('Location: login.php');
// }

$select = $bdd->prepare("SELECT * FROM categories WHERE id='$id'");
$select->execute(array($id));
// Edit category selected
if ($select->rowCount() == 1) {

	$result = $select->fetch();

	$category = $result['category'];


	if (isset($_POST['proBtn'])) {


		if (!empty($_POST['category'])) {

			$category = htmlspecialchars($_POST['category']);

			$update = $bdd->prepare("UPDATE categories SET category = ? where id = ?");
			$update->execute(array($category, $id));

			header('Location: category.php');
		} else { }
	}
}
?>

<div id="wrapper">

	<!-- Sidebar -->
	<ul class="sidebar navbar-nav">
		<li class="nav-item ">
			<a class="nav-link" href="../index.php">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Dashboard</span>
			</a>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-user"></i>
				<span>Edit Profile</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item" href="../editpic.php">Edit Picture</a>
				<a class="dropdown-item" href="../editprofile.php">Edit Infos</a>
				<a class="dropdown-item" href="../editpwd.php">Edit Password</a>
			</div>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-users"></i>
				<span>Users</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item" href="../vendor.php">View Vendor</a>
				<a class="dropdown-item" href="../customer.php">View Customer</a>
			</div>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="../reports.php">
				<i class="fa fa-thumbs-down"></i>
				<span>Report Scam</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="../orders.php">
				<i class="fa fa-btc"></i>
				<span>Orders</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="../becomeaseller.php">
				<i class="fas fa-cart-plus"></i>
				<span>Become a seller</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="../tickets.php">
				<i class="fa fa-life-ring"></i>
				<span>Support</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="../articles.php">
				<i class="fa fa-cart-arrow-down"></i>
				<span>Articles</span></a>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="category.php" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-folder"></i>
				<span>Category</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item active" href="addcategory.php">Add Category</a>
				<a class="dropdown-item" href="category.php">View Categories</a>
			</div>
		</li>
	</ul>

	<div id="content-wrapper">

		<div class="container-fluid">

			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="#">Categories</a>
				</li>
				<li class="breadcrumb-item active">Edit Category</li>
			</ol>

			<div class="container col-lg-6 col-sm-4 ">
				<form method="POST">
					<?php echo @$errors; ?>
					<?php echo @$success; ?>
					<div class="form-group">
						<label>Category Name</label>
						<input type="text" class="form-control" name="category" value="<?= ((isset($category)) ? $category : '') ?>" required="required">
					</div>
					<input type="submit" name="proBtn" class="btn btn-dark btn-block" value="Edit Category">
				</form>
			</div>
		</div>

		<!-- /.container-fluid -->

		<!-- Sticky Footer -->


	</div>
	<!-- /.content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
	<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
				<button class="close" type="button" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
				<a class="btn btn-dark" href="logout.php">Logout</a>
			</div>
		</div>
	</div>
</div>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>

<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demo.js"></script>

</body>