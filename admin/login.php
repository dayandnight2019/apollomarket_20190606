<?php session_start(); include('../includes/config.php');


//if(isset($_SESSION['member_id'])) {
//    header('Location: index.html');
//}

//if(isset($_SESSION['id'])) {
 //   header('Location: ../index.html');
//}



// login request script
if(isset($_POST['loginBtn']) && isset($_POST['captcha'])) {
	$email = htmlspecialchars($_POST['email']);
	$password = htmlspecialchars($_POST['password']);
	$password = hash('sha256', $password);

	if( empty($email) or empty($password)) {
		$errors = '<div class="alert alert-danger">They are fields empty.</div>';
	}else{
		$select = $bdd->prepare("SELECT * FROM adminuser WHERE email = ? and password = ?");
		$select->execute(array($email,$password));
		if ($select->rowCount()==1) {
			if($_POST['captcha'] == $_SESSION['captcha']) {
				$result = $select->fetch();
				$_SESSION['member_id'] = $result['id'];
				$_SESSION['username'] = $result['username'];
				header ("Location: index.php");
			}else{
				$errors = '<div class="alert alert-danger">Captcha Not Good !</div>';
			}
		} else {
			$errors = '<div class="alert alert-danger">Email and pasword combination does not match.</div>';
		}
	}
}


?>

<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>LogIn - Admin Panel</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">

  </head>

  <body class="bg-dark">

    <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Login</div>
        <div class="card-body">
          <form method="POST">
              <?php echo @$errors; ?>
            <div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email address" required="required" autofocus="autofocus">
                <label for="inputEmail">Email address</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" required="required">
                <label for="inputPassword">Password</label>
              </div>
            </div>
                        <div class="form-group">
            <div style="float:left;" class="col-sm-5">
              <img src="captcha.php" />
            </div>
            <div style="float:right;" class="col-sm-7">
              <input class="form-control"  type="text" name="captcha" />
            </div>
            <br />
            <br />
          </div>
            <input type="submit" name="loginBtn" class="btn btn-dark btn-block" value="Login" >
          </form>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  </body>

</html>
