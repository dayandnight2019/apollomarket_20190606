<?php include('header.php');

$member = $bdd->query("SELECT * FROM members WHERE vendor='0'");
$members = $member->rowCount();

$vendor = $bdd->query("SELECT * FROM members WHERE vendor='1' or vendor='2' or vendor='3'");
$vendors = $vendor->rowCount();

$order = $bdd->query("SELECT * FROM orders");
$orders = $order->rowCount();

$product = $bdd->query("SELECT * FROM articles");
$products = $product->rowCount();

$sale = $bdd->query("SELECT SUM(sales) AS allsales FROM members WHERE vendor='1'");
$sales = $sale->fetch();

// show all sales in website
$num_sales = $sales['allsales'];

// show all reports in website
$report = $bdd->query("SELECT * FROM reports");
$report = $report->rowCount();

$adminUser = $bdd->query("SELECT * FROM adminuser");
$adminUserBtcApi = $adminUser->fetch();

$apiKey = "".$adminUserBtcApi['api_key']."";
$version = 2; // API version
$pin = "".$adminUserBtcApi['secret_pin']."";
$block_io = new BlockIo($apiKey, $pin, $version);


$balanceShow = $block_io->get_address_balance(array('addresses' => ''.$adminUserBtcApi['btc_address'].''));




?>

    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>

          <!-- Icon Cards-->
          <div class="row">
          	<div class="col-xl-4  mb-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fa fa-btc"></i>
                  </div>
                  <div class="mr-5"><h3><?php echo $balanceShow->data->available_balance; ?> <i class="fa fa-btc"></i></h3></div>
                </div>
              </div>
            </div>
            <div class="col-xl-4  mb-3">
              <div class="card text-white bg-warning o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa fa-vcard"></i>
                  </div>
                  <div class="mr-5"><?=$vendors?> Vendor(s)</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="vendor.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-3">
              <div class="card text-white bg-success o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                  </div>
                  <div class="mr-5"><?=$orders?> Order(s)</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="orders.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-3">
              <div class="card text-white bg-dark o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa fa-users"></i>
                  </div>
                  <div class="mr-5"><?=$members?> Customer(s)</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="customer.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-xl-4 col-sm-6 mb-3">
              <div class="card text-white bg-info o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa fa-dollar"></i>
                  </div>
                  <div class="mr-5"><?=$num_sales?> Sales</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="orders.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
              </div>
                <div class="col-xl-4 col-sm-6 mb-3">
              <div class="card text-white bg-danger o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="fas fa-fw fa fa-thumbs-down"></i>
                  </div>
                  <div class="mr-5"><?=$report?> Report(s)</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="reports.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>

              <div class="col-xl-4 col-sm-6 mb-3">
              <div class="card text-white bg-secondary o-hidden h-100">
                <div class="card-body">
                  <div class="card-body-icon">
                    <i class="	fa fa-download"></i>
                  </div>
                  <div class="mr-5"><?=$products?> Product(s)</div>
                </div>
                <a class="card-footer text-white clearfix small z-1" href="articles.php">
                  <span class="float-left">View Details</span>
                  <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                  </span>
                </a>
              </div>
            </div>

        </div>
            <h3><i class="fa fa-vcard"></i> | New Sellers</h3>
            <hr />
        <!-- /.container-fluid -->
            <?php $allvend = $bdd->query("SELECT * FROM members WHERE vendor='1' ORDER BY id DESC LIMIT 0,4");
                    while($row = $allvend->fetch()) {

                            $totalPrice = 0;

                            $vend_id = $row['id'];
                            $vend_user = $row['username'];
                            $vend_pic = $row['ppicture'];
                            $vend_email = $row['email'];
                            $vend_sales = $row['sales'];
                            $vend_status = $row['status'];
                            $vend_btc = $row['btc_address'];
                            $vend_earn = $row['earnings'];

                            $totalEarnings = $vend_earn * 0.80;



                        echo'<div style="float:left;margin-right:10px;" class="card" >';
                        	if(empty($vend_pic)) {
                        		echo'<img style="margin-right:auto;margin-left:auto;display:block;width:50%" class="card-img-top imgAdmin" src="../members/images/default.png" alt="Card image cap" height="200px" width="200px">';
                        	}else{
                              echo'<img class="card-img-top imgAdmin" src="../members/uploads/'.$vend_pic.'" alt="Card image cap" height="200px" width="200px">';
                              }
                              echo'<div class="card-body">
                                <h5 class="card-title text-center">'.$vend_user.'</h5>
                                <p class="card-text"><i class="fa fa-envelope"></i> : '.$vend_email.'<br />
                                                     <i class="fa fa-shopping-cart"></i> : '.$totalEarnings.' $<br />
                                                     <i class="fa fa-btc"></i> : <span class="badge badge-dark">'.$vend_btc.'</span><br />
                                </p>
                                <a href="../members/member.php?username='.$vend_user.'&id='.$vend_id.'&pn=1" class="btn btn-dark btn-block">View Profile</a>
                              </div>
                            </div>';


                          }


            ?>




      </div>
      <!-- /.content-wrapper -->
      <hr />
        <?php

        	//if(isset($_POST['sendBTC'])) {

        		//$address_btc = mysqli_real_escape_string($connection, $_REQUEST['address_btc']);
        		//$price_btc = mysqli_real_escape_string($connection, $_REQUEST['price_btc']);

            //$block_io->withdraw_from_addresses(array('amounts' => ''.$price_btc.'', 'from_addresses' => '39e3H2EmdrxHuVCbtjYJnNPMJwP91cgB49', 'to_addresses' => ''.$address_btc.''));

            	//$success = "Money send with success :";


//}


  //$options = '';
  //$categories = mysqli_query($connection, "SELECT * FROM members WHERE vendor='1'");

  // show list of categories
  //while( $rs = mysqli_fetch_array($categories) ){
    //$options .= '<option>'.$rs['btc_address'].'</option>';
    //}


     ?>

      	<!--<div class="col-lg-6  col-sm-4">
      		<div class="container">
      			<?php //echo @$success; ?>
      		<h3><i class="fa fa-btc"></i> Payments seller</h3>
      		<hr />
            <form method="POST">
            	<select class="form-control"  name="address_btc">
               <?php

               //echo $options;

               ?>
              </select>
            	<input class="form-control" type="text" name="price_btc" placeholder="price in BTC">
            	<br />
            	<button class="btn btn-dark" name="sendBTC">Send <i class="fa fa-btc"></i></button>
            </form>
         </div>
     </div>-->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-dark" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
