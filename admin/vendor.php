<?php include('header.php');


// delete vendor of website
if(isset($_POST['supBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = $bdd->prepare("DELETE FROM members WHERE id = ?");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'vendor.php'</script>";

    $delete = "<div class='alert alert-success'>Vendor delete !</div>";

}

if(isset($_POST['vendorfieldZero'])) {

  $id_rdv = htmlspecialchars($_POST['id']);

  $delete_query = $bdd->prepare("UPDATE members SET earnings='0' WHERE id = ?");
  $delete_query->execute(array($id_rdv));

  echo"<script>window.location 'vendor.php'</script>";

  $delete = "<div class='alert alert-success'>Vendor 0 $ done !</div>";

}

if(isset($_POST['unLockBtn'])) {

  $id_rdv = htmlspecialchars($_POST['id']);

  $delete_query = $bdd->prepare("UPDATE members SET vendor='1' WHERE id = ?");
  $delete_query->execute(array($id_rdv));

  $lock_user =  $bdd->prepare("DELETE FROM locks WHERE user_lock = ?");
  $lock_user->execute(array($id_rdv));

  echo"<script>window.location 'vendor.php'</script>";

  $delete = "<div class='alert alert-success'>Vendor Unlock !</div>";

}


if(isset($_POST['sellerPaidBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = $bdd->prepare("UPDATE orders SET send='3' WHERE id = ? ");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'orders.php'</script>";

    $delete = "<div class='alert alert-success'>Order deleted !</div>";

}


?>
<div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>

        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">All vendors</a>
            </li>
            <li class="breadcrumb-item active">Vendors</li>
          </ol>

         <div class="card mb-3">
            <div class="card-header">
              <i class="fa fa-btc"></i> |
              Vendor from Markety</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Username</th>
                      <th>Email</th>
                      <th>BTC Address</th>
                      <th>Delete</th>
                      <th>View</th>
                      <th>Total Earnings</th>
                      <th>Paid</th>
                      <th>Done</th>
                      <!--<th>Lock Vendor</th>-->
                    </tr>
                  </thead>
                  <tbody>
             <?php

                $vendor = $bdd->query("SELECT * FROM members WHERE vendor='1' or vendor='2' or vendor='3' ORDER BY id DESC" );

                      while($row = $vendor->fetch()) {



                           $totalPrice = 0;

                          $id_vend =    $row['id'];
                          $user_vend = $row['username'];
                          $mail_vend = $row['email'];
                          $btc_vend = $row['btc_address'];
                          $vend_status =  $row['vendor'];
                          $sales_vendor = $row['sales'];
                          $vend_earn  = $row['earnings'];

                          $totalPrice = $vend_earn * 0.90;




                      echo'<tr>';
                      echo'<td>'.$user_vend.'</td>';
                      echo'<td>'.$mail_vend.'</td>';
                      echo'<td>'.$btc_vend.'</td>';

                         ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_vend ?>">

                    <?php

                      echo'<td><button name="supBtn" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button></td>';
                    ?>

                    </form>

                    <?php
                      echo'<td><a href="../members/member.php?username='.$user_vend.'&id='.$id_vend.'&pn=1"><button class="btn btn-dark"><i class="fas fa-eye"></i> View</button></a></td>';
                      echo '<td>'.$totalPrice.' $</td>';

                          ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_vend ?>">

                    <?php
                      echo'<td><a href="pay_seller.php?id='.$id_vend.'" class="btn btn-success">Pay in <i class="fa fa-btc"></i></a></td>';
                      echo'<td><button class="btn btn-dark" name="vendorfieldZero">0 $ now !</button></td>';
                      ?>

                    </form>

                    <?php



                      echo'</tr>';
                                    }





            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->


      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-dark" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
