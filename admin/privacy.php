<?php
	include('header.php');

	$select = $bdd->query("SELECT * FROM privacy ");

	if( $select->rowCount()==1 ){

		$result = $select->fetch();

		$title = $result['title'];
		$text_privacy = $result['text_privacy'];

		if( !empty($_POST['title']) and !empty($_POST['text_privacy'])  ){

			$title = htmlspecialchars($_POST['title']);
			$text_privacy =  $_POST['text_privacy'];

			$update = $bdd->prepare("UPDATE privacy SET title = ?,text_privacy = ?");
			$update->execute(array($title,$text_privacy));

			$success = "<div class='alert alert-success'>Updated successfully</div>";


		}

	}else{

		if( isset($_POST['addPrivacyContents']) ){

      $title = htmlspecialchars($_POST['title']);
			$text_privacy = $_POST['text_privacy'];




			if( !empty($title) and !empty($text_privacy)){



				$insert = $bdd->prepare("INSERT INTO privacy (title,text_privacy,date_added) VALUES(?,?,NOW())");
				$insert->execute(array($title,$text_privacy));

				$success = "<div class='alert alert-success'>Saved with success !</div";


				}else{

					$errors .= "<div class='alert alert-danger'>Please fill any one of the field </div>";

				}
			}

	}

?>
<div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>


  <div id="content-wrapper">

    <ol class="breadcrumb">
      <li class="breadcrumb-item">
        <a href="#">Term and condition</a>
      </li>
      <li class="breadcrumb-item active">Edit Term and condition</li>
    </ol>

      <div class="container-fluid">
<script src="../articles/ckeditor/ckeditor.js"></script>
    <h1 class="text-center">Term and condition</h1>
  <div class="container col-lg-12 col-sm-4 ">
<form method="POST" enctype="multipart/form-data">
    <?php echo @$success; ?>
    <?php echo @$errors; ?>
    <div class="form-group">
        <label for="inputEmail">Title</label>
              <div class="form-label-group">
                <input type="text"  class="form-control" name="title" value="<?=((isset($title) )?$title:'')?>" placeholder="Privacy Title"  required="required" autofocus="autofocus">
              </div>
    </div>
    <div class="form-group">
      <label>Text</label>
            <textarea style="min-height:200px;" id="editor1"  class="form-control" name="text_privacy"  required="required">
              <?=((isset($text_privacy) )?$text_privacy:'')?>
            </textarea>
    </div>
    <script>
        CKEDITOR.replace( 'editor1' );
    </script>

            <input class="btn btn-dark btn-block" type="submit" name="addPrivacyContents" value="Saved" />
</form>
</div>
    </div>
    </div>
    </div>
