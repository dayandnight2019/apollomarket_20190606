<?php include('header.php');

$id = htmlspecialchars($_GET['id']);

$ticket_show = mysqli_query($connection, "SELECT * FROM support WHERE id='$id'");
$ticket_shower = mysqli_fetch_array($ticket_show);

$usernameSupport = $ticket_shower['username'];

$userTicket = mysqli_query($connection, "SELECT * FROM members WHERE username='$usernameSupport'");
$userTickets = mysqli_fetch_array($userTicket);

$adminId = $admin_picture['id'];
$picAdmin = $admin_picture['ppicture'];

if(isset($_POST['replyBtn'])) {

  $reply =  htmlspecialchars($_POST['reply']);

  $insert_reply = ("INSERT INTO tickets  (user_id,support_id,text_support,date_added) VALUES(?,?,?,NOW())");
  $insert_reply->execute(array($adminId,$id,$reply));


}


if(isset($_POST['closeBtn'])) {

  $insert_status = $bdd->prepare("UPDATE support SET close='1' where id = ?");
  $insert_status->execute(array($id));

  $success_reply = "<div class='alert alert-success'>Ticket closed !</div>";
  header('Location: tickets.php');
}

?>


    <div id="wrapper">

      <!-- Sidebar -->
      <ul class="sidebar navbar-nav">
        <li class="nav-item ">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-user"></i>
            <span>Edit Profile</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item " href="editpic.php">Edit Picture</a>
            <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
            <a class="dropdown-item" href="editpwd.php">Edit Password</a>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-users"></i>
            <span>Users</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="vendor.php">View Vendor</a>
            <a class="dropdown-item" href="customer.php">View Customer</a>
          </div>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="reports.php">
            <i class="fa fa-thumbs-down"></i>
            <span>Report Scam</span></a>
        </li>
          <li class="nav-item">
          <a class="nav-link" href="orders.php">
            <i class="fa fa-btc"></i>
            <span>Orders</span></a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="becomeaseller.php">
          <i class="fas fa-cart-plus"></i>
          <span>Become a seller</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
        <li class="nav-item">
        <a class="nav-link" href="tickets.php">
          <i class="fa fa-life-ring"></i>
          <span>Support</span></a>
      </li>
          <li class="nav-item">
          <a class="nav-link" href="articles.php">
            <i class="fa fa-cart-arrow-down"></i>
            <span>Articles</span></a>
        </li>
          <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Category</span>
          </a>
            <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
            <a class="dropdown-item" href="categories/category.php">View Categories</a>
          </div>
        </li>
      </ul>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Support</a>
            </li>
            <li class="breadcrumb-item active">Reply</li>
          </ol>

        <div class="container">
          <h5 class="text-center">Reply ticket #<?=$ticket_shower['id']?> </h5>
      <div style="max-height:500px;overflow-x:hidden;">
          <div class="row feedCustomer">
            <div class="col-md-1">
              <img style="border-radius:200px;" src="../members/uploads/<?= $userTickets['ppicture'] ?>" height="50px" >
            </div>
            <div class="col-md-11">
            <p><span class="badge badge-success"><?= $ticket_shower['username'] ?></span></p>
              <p><?= $ticket_shower['text_last'] ?></p>
            </div>
          </div>
        <?php  $ticket_usr = $bdd->prepare("SELECT * FROM tickets WHERE support_id = ? ORDER BY id ASC");
               $ticket_usr->execute(array($id));
                    while($row = $ticket_usr->fetch()) {

                              $ticket_user_id = $row['user_id'];
                              $text_ticket = $row['text_support'];


              $user_ticket = $bdd->prepare("SELECT * FROM members WHERE id = ?");
              $user_ticket->execute(array($ticket_user_id));

                    while($row1 = $user_ticket->fetch()) {
                            $username_ticket = $row1['username'];
                            $pic_ticket = $row1['ppicture'];

                      echo'<div class="row feedCustomer">';
                        echo'<div class="col-md-1">';
                      if($row['user_id']==1) {
                          echo'<img style="border-radius:200px;" src="uploads/'.$picAdmin.'" height="50px" >';
                        }else{
                          echo'<img style="border-radius:200px;" src="../members/uploads/'.$pic_ticket.'" height="50px" >';
                        }
                        echo'</div>';
                        echo'<div class="col-md-11">';
                      if($row['user_id']==1) {
                        echo'<p><span class="badge badge-dark">Administrator</span></p>';
                      }else{
                          echo'<p><span class="badge badge-success">'.$username_ticket.'</span></p>';
                        }
                          echo'<p>'.$text_ticket.'</p>';
                        echo'</div>';
                      echo'</div>';

                          }

                    }




        ?>
      </div>
        <br />
        <?php echo @$success_reply; ?>
        <script src="../ckeditor/ckeditor.js"></script>
        <form method="POST">
          <?php if($ticket_shower['close']==1) { ?>

            <textarea style="min-height:200px;" type="text" class="form-control" disabled></textarea>
          <br />
          <input type="submit" class="btn btn-dark btn-block" value="Ticket CLose" disabled>

          <?php }else{ ?>

          <textarea style="min-height:200px;" type="text" id="editor1" class="form-control" name="reply" ></textarea>
          <br />
          <input type="submit" class="btn btn-dark btn-block" name="replyBtn" value="Reply">

          <input type="submit" class="btn btn-danger btn-block" name="closeBtn" value="Close ticket now ">

        <?php } ?>

        </form>

        </div>
      </div>
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
              <a class="btn " href="logout.php">Logout</a>
            </div>
          </div>
        </div>
      </div>

      <script src="vendor/jquery/jquery.min.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Page level plugin JavaScript-->
      <script src="vendor/chart.js/Chart.min.js"></script>
      <script src="vendor/datatables/jquery.dataTables.js"></script>
      <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="js/sb-admin.min.js"></script>

      <!-- Demo scripts for this page-->
      <script src="js/demo/datatables-demo.js"></script>
      <script src="js/demo/chart-area-demo.js"></script>
