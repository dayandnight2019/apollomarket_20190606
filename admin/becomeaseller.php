<?php
include('header.php');

$admin_id = $_SESSION['member_id'];


$select = $bdd->prepare("SELECT * FROM become_seller where admin_id = ?");
$select->execute(array($admin_id));

if ($select->rowCount() == 1) {

	$result = $select->fetch();

	$title = $result['title'];
	$description = $result['description'];
	$price = $result['price'];

	if (!empty($_POST['title']) and !empty($_POST['description'])  and !empty($_POST['price'])) {

		$title = htmlspecialchars($_POST['title']);
		$description = $_POST['description'];
		$price = htmlspecialchars($_POST['price']);

		$update = $bdd->prepare("UPDATE become_seller SET title = ?,description = ?,price = ? where admin_id = ?");
		$update->execute(array($title, $description, $price, $admin_id));

		$success = "<div class='alert alert-success'>Updated successfully</div>";
	}
} else {

	if (isset($_POST['addBecomeSeller'])) {

		$title = htmlspecialchars($_POST['title']);
		$description = $_POST['description'];
		$price = htmlspecialchars($_POST['price']);

		if (!empty($title) and !empty($description) and !empty($price)) {

			$insert = ("INSERT INTO become_seller (admin_id,title,description,price,date_added) VALUES(?,?,?,?,NOW())");
			$insert->execute(array($admin_id, $title, $description, $price));

			$success = "<div class='alert alert-success'>Saved !</div";
		} else {

			$errors .= "<div class='alert alert-danger'>Please fill any one of the field </div>";
		}
	}
}

?>
<div id="wrapper">

	<!-- Sidebar -->
	<ul class="sidebar navbar-nav">
		<li class="nav-item ">
			<a class="nav-link" href="index.php">
				<i class="fas fa-fw fa-tachometer-alt"></i>
				<span>Dashboard</span>
			</a>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-user"></i>
				<span>Edit Profile</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item" href="editpic.php">Edit Picture</a>
				<a class="dropdown-item" href="editprofile.php">Edit Infos</a>
				<a class="dropdown-item" href="editpwd.php">Edit Password</a>
			</div>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-users"></i>
				<span>Users</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item" href="vendor.php">View Vendor</a>
				<a class="dropdown-item" href="customer.php">View Customer</a>
			</div>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="reports.php">
				<i class="fa fa-thumbs-down"></i>
				<span>Report Scam</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="orders.php">
				<i class="fa fa-btc"></i>
				<span>Orders</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="becomeaseller.php">
				<i class="fas fa-cart-plus"></i>
				<span>Become a seller</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="privacy.php">
				<i class="  fa fa-balance-scale"></i>
				<span>Terms and condition</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="tickets.php">
				<i class="fa fa-life-ring"></i>
				<span>Support</span></a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="articles.php">
				<i class="fa fa-cart-arrow-down"></i>
				<span>Articles</span></a>
		</li>
		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-fw fa-folder"></i>
				<span>Category</span>
			</a>
			<div class="dropdown-menu" aria-labelledby="pagesDropdown">
				<a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
				<a class="dropdown-item" href="categories/category.php">View Categories</a>
			</div>
		</li>
	</ul>


	<div id="content-wrapper">

		<ol class="breadcrumb">
			<li class="breadcrumb-item">
				<a href="#">Become a seller</a>
			</li>
			<li class="breadcrumb-item active">Edit become a seller</li>
		</ol>

		<div class="container-fluid">
			<script src="../articles/ckeditor/ckeditor.js"></script>
			<h1 class="text-center">Become a seller</h1>
			<div class="container col-lg-12 col-sm-4 ">
				<form method="POST" enctype="multipart/form-data">
					<?php echo @$success; ?>
					<?php echo @$errors; ?>
					<div class="form-group">
						<label for="inputEmail">Title</label>
						<div class="form-label-group">
							<input type="text" class="form-control" name="title" value="<?= ((isset($title)) ? $title : '') ?>" placeholder="Title of product" required="required" autofocus="autofocus">
						</div>
					</div>
					<div class="form-group">
						<label>Description</label>
						<textarea style="min-height:200px;" id="editor1" class="form-control" name="description" required="required">
              <?= ((isset($description)) ? $description : '') ?>
            </textarea>
					</div>
					<script>
						CKEDITOR.replace('editor1');
					</script>
					<div class="form-group">
						<label>Price</label>
						<input type="number" class="form-control" value="<?= ((isset($price)) ? $price : '') ?>" name="price" required="required" />
					</div>

					<input class="btn btn-dark btn-block" type="submit" name="addBecomeSeller" value="Saved" />
				</form>
			</div>
		</div>
	</div>
</div>