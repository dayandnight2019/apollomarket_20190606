<?php include('header.php');



// Delete Reports request

if(isset($_POST['supBtn'])){

    $id_rdv = htmlspecialchars($_POST['id']);

    $delete_query = $bdd->prepare("DELETE FROM reports WHERE id = ? ");
    $delete_query->execute(array($id_rdv));

    echo"<script>window.location 'reports.php'</script>";

    $delete = "<div class='alert alert-success'>Report deleted !</div>";

}




?>
<div id="wrapper">

      <!-- Sidebar -->
       <ul class="sidebar navbar-nav">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">
                  <i class="fas fa-fw fa-tachometer-alt"></i>
                  <span>Dashboard</span>
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-user"></i>
                  <span>Edit Profile</span>
                </a>
                  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <a class="dropdown-item " href="editpic.php">Edit Picture</a>
                  <a class="dropdown-item" href="editprofile.php">Edit Infos</a>
                  <a class="dropdown-item" href="editpwd.php">Edit Password</a>
                </div>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-users"></i>
                  <span>Users</span>
                </a>
                  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <a class="dropdown-item" href="vendor.php">View Vendor</a>
                  <a class="dropdown-item" href="customer.php">View Customer</a>
                </div>
              </li>
                <li class="nav-item">
                <a class="nav-link" href="reports.php">
                  <i class="fa fa-thumbs-down"></i>
                  <span>Report Scam</span></a>
              </li>
                <li class="nav-item">
                <a class="nav-link" href="orders.php">
                  <i class="fa fa-btc"></i>
                  <span>Orders</span></a>
              </li>
              <li class="nav-item">
              <a class="nav-link" href="becomeaseller.php">
                <i class="fas fa-cart-plus"></i>
                <span>Become a seller</span></a>
            </li>
            <li class="nav-item">
        <a class="nav-link" href="privacy.php">
          <i class="  fa fa-balance-scale"></i>
          <span>Terms and condition</span></a>
      </li>
              <li class="nav-item">
              <a class="nav-link" href="tickets.php">
                <i class="fa fa-life-ring"></i>
                <span>Support</span></a>
            </li>
                <li class="nav-item">
                <a class="nav-link" href="articles.php">
                  <i class="fa fa-cart-arrow-down"></i>
                  <span>Articles</span></a>
              </li>
                <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-fw fa-folder"></i>
                  <span>Category</span>
                </a>
                  <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                  <a class="dropdown-item" href="categories/addcategory.php">Add Category</a>
                  <a class="dropdown-item" href="categories/category.php">View Categories</a>
                </div>
              </li>
            </ul>


      <div id="content-wrapper">

        <div class="container-fluid">


          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="#">Reports</a>
            </li>
            <li class="breadcrumb-item active">View Reports</li>
          </ol>

            <!-- ORDERS TABLE -->

         <div class="card mb-3">
             <?php echo @$delete; ?>
            <div class="card-header">
              <i class="fa fa-btc"></i> |
              Reports from Markety</div>
            <div class="card-body">


              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Send By</th>
                      <th>For</th>
                      <th>Reason</th>
                      <th>Description</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
             <?php

                $reports = $bdd->query("SELECT * FROM reports" );

                      while($row = $reports->fetch()) {

                          $id_rep =    $row['id'];
                          $user_from = $row['user_from'];
                          $user_to = $row['user_to'];
                          $report_txt = $row['report_txt'];
                          $rep_reason = $row['reason'];

                $user_from_rep = $bdd->prepare("SELECT * FROM members WHERE id = ?");
                $user_from_rep->execute(array($user_from));

                      while($row1 = $user_from_rep->fetch()) {
                          $user_to_report = $row1['username'];


                      echo'<tr>';
                      echo'<td>'.$id_rep.'</td>';
                      echo'<td>'.$user_to_report.'</td>';
                      echo'<td>'.$user_to.'</td>';
                      echo'<td>'.$rep_reason.'</td>';
                      echo'<td>'.$report_txt.'</td>';

                            ?>
                    <form method="POST">
                        <input type="hidden" name="id" value="<?php echo $id_rep ?>">

                    <?php

                      echo'<td><button name="supBtn" type="submit" class="btn btn-danger"><i class="fas fa-trash-alt"></i> Delete</button></td>';
                    ?>
                    </form>

                    <?php
                      echo'</tr>';

                            }
                        }

            ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <!-- /.container-fluid -->

        <!-- Sticky Footer -->


      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-dark" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

  </body>

</html>
