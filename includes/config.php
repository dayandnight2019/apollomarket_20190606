<?php

$host = 'localhost';
$user = 'root';
$pass = '';
// $user = 'apollomarket';
// $pass = 'RGbi3LjfcMQmzqey';
$name = 'apollomarket';

$connection = mysqli_connect($host,$user,$pass,$name); // Procedural MySql connection

try {
    $bdd = new PDO('mysql:host='.$host.';dbname='.$name, $user, $pass);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
