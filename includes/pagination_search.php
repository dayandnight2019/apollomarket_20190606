<?php

	include("config.php");

// Pagination search Script

  $q = htmlspecialchars(@$GET['q']);

	$query= $bdd->query("SELECT count(id) from `articles` WHERE title LIKE '%".$q."%' ");
	$row = $query->rowCount();

	$rows = $row[0];

	$page_rows = 6;

	$last = ceil($rows/$page_rows);

	if($last < 1){
		$last = 1;
	}

	$pagenum = 1;

	if(isset($_GET['pn'])){
		$pagenum = preg_replace('#[^0-9]#', '', htmlspecialchars($_GET['pn']));
	}

	if ($pagenum < 1) {
		$pagenum = 1;
	}
	else if ($pagenum > $last) {
		$pagenum = $last;
	}

	$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

	$nquery = $bdd->query("select * from `articles` $limit");

	$paginationCtrls = '';

	if($last != 1){

	if ($pagenum > 1) {
        $previous = $pagenum - 1;
		$paginationCtrls .= '<a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?q='.$q.'&pn='.$previous.'" class="btn btn-dark">Previous</a> &nbsp; &nbsp; ';

		for($i = $pagenum-4; $i < $pagenum; $i++){
			if($i > 0){
		        $paginationCtrls .= '<a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?q='.$q.'&pn='.$i.'" class="btn btn-dark">'.$i.'</a> &nbsp; ';
			}
	    }
    }

	$paginationCtrls .= ''.$pagenum.' &nbsp; ';

	for($i = $pagenum+1; $i <= $last; $i++){
		$paginationCtrls .= '<a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?q='.$q.'&pn='.$i.'" class="btn btn-dark">'.$i.'</a> &nbsp; ';
		if($i >= $pagenum+4){
			break;
		}
	}

    if ($pagenum != $last) {
        $next = $pagenum + 1;
        $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?q='.$q.'&pn='.$next.'" class="btn btn-dark">Next</a> ';
    }
	}

?>
