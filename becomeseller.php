<?php include('header.php');

if(!isset($_SESSION['id'])) {
    header('Location: members/login.php');
}

if(@$rsuserInfo['vendor'] >= 1) {
    header('Location: index.php');
}


// Script for to add Vendor in 'Free Seller'

//if(isset($_REQUEST['sellerFree'])) {

    //$insert_member = "UPDATE members SET
                      //vendor='1'
                      //where id='$user_id'
    //";

    //mysqli_query($connection, $insert_member);

    //$success = "<div class='alert alert-success'>You're now Seller for 0 $/month !</div>";

//}

$become_seller = $bdd->query("SELECT * FROM become_seller");
$become_seller_show = $become_seller->fetch();


$admin_use = $bdd->query("SELECT * FROM adminuser");
$admin_user = $admin_use->fetch();

$admin_btc = $admin_user['btc_address'];

$source2 = "https://blockchain.info/tobtc?currency=USD&value=".$become_seller_show['price']."";
$file1 = file_get_contents($source2);
$btc1 = json_decode($file1, true);

?>


<br>
<br>
<div class="container">
        <div class="col-lg-12 text-center">
            <h1>Become a Seller</h1>
            <p class="lead">To become a seller please click on button</p>
        </div>
    <?php echo @$success; ?>
    <hr class="mb-4" />
    <div class="card-deck mb-3 text-center">


        <div class="card mb-4 box-shadow">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal"><?=$become_seller_show['title']?></h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title"><?=$become_seller_show['price']?> $</h1>
            <ul class="list-unstyled mt-3 mb-4">
              <?=$become_seller_show['description']?>
            </ul>
            <a href="payment_bseller.php?id=<?=$become_seller_show['id']?>" class="btn btn-lg btn-success">Become a Seller</a>
          </div>
        </div>

      </div>
    </div>

</div>
