<?php include('header.php');
include('../includes/pagination_category.php');

$cat = $bdd->query("SELECT * FROM categories");

$cate = htmlspecialchars(@$_GET['cat']);

$arti = htmlspecialchars(@$_GET['art']);

$categ = $bdd->prepare("SELECT * FROM articles WHERE category = ?");
$categ->execute(array($cate));
$categor = $categ->fetch();

$catego = $bdd->prepare("SELECT * FROM categories WHERE id = ?");
$catego->execute(array($cate));
$cat_show = $catego->fetch();

// Delete product Button
if (isset($_POST['deleteProduct'])) {

	$id_rdv = htmlspecialchars($_POST['id']);

	$delete_article = $bdd->prepare("DELETE FROM articles where id = ?");
	$delete_article->execute(array($id_rdv));

	$delete_success = '<div class="alert alert-success">Product delete with success !</div>';
}


// Add to Cart Button
if (isset($_POST['addCartBtn'])) {

	$order_article = rand(999, 50000);

	$insert_cart = $bdd->prepare("INSERT INTO orders (user_from,article_id,order_article,date_added) VALUES(?,?,?,NOW())");
	$insert_cart->execute(array($user_id, $arti, $order_article));

	$success = '<div class="alert alert-success">Product added to shopping cart !</div>';
} else { }

?>

<!-- Page Content -->
<div class="container">
	<?php echo @$success; ?>
	<div class="row">

		<div class="col-lg-3">
			<h6 class="my-4"></h6>
			<div class="list-group">
				<a class="list-group-item active" style="color:white;"><i class="fas fa-folder-open"></i> Categories</a>
				<?php while ($row = $cat->fetch()) {

					$id_ca =  $row['id'];
					$catego = $row['category'];

					$cat_number = $bdd->prepare("SELECT * FROM articles WHERE category = ?");
					$cat_number->execute(array($id_ca));
					$row1 = $cat_number->rowCount();

					echo '<a href="category-' . $id_ca . '-1.html" class="list-group-item">' . $catego . '<p style="font-size:12px;color:white;display:inline-flex;border-radius:50px;background-color:#343a40;float:right;padding:4px;">' . $row1 . '</p></a>';
				} ?>
			</div>

		</div>
		<!-- /.col-lg-3 -->

		<div class="col-lg-9">
			<br />
			<div style="background-color:#000;color:white;" class="card">
				<h3 class="text-center"><?= $cat_show['category'] ?> </h3>
			</div>
			<div class="row">
				<?php

				$product = $bdd->prepare("SELECT * FROM articles WHERE category = ? $limit ");
				$product->execute(array($cate));

				// show article only with category choose
				while ($row = $product->fetch()) {

					$id_article =   $row['id'];
					$user_article = $row['user_id'];
					$tit_article = $row['title'];
					$descri_article = $row['description'];
					$pri_article = $row['price'];
					$pic_article = $row['ppicture'];

					$source2 = "https://blockchain.info/tobtc?currency=USD&value=" . $pri_article . "";
					$file1 = file_get_contents($source2);
					$btc1 = json_decode($file1, true);

					$vend = $bdd->prepare("SELECT * FROM members WHERE id = ?");
					$vend->execute(array($user_article));

					while ($row1 = $vend->fetch()) {
						$vend_id = $row1['id'];
						$vend_user = $row1['username'];
						$vend_pic = $row1['ppicture'];

						echo '<div style="margin-top:20px;" class="col-lg-4 col-md-6 mb-4">';
						echo '<div class="card h-100">';
						echo '<a href="../articles/article.php?id=' . $id_article . '"><img class="card-img-top" src="../articles/uploads/' . $pic_article . '" alt=""  height="500px" width="500px" ></a>';
						echo '<div class="card-body">';
						echo '<h6 class="card-title text-center">';
						echo '<a href="../articles/article.php?id=' . $id_article . '">' . $tit_article . '</a>';
						echo '</h6>';
						echo '<h6 class="text-center">Shipping : ' . $row['shipping_country'] . '</h6>';
						echo '<h6 class="text-center">' . $pri_article . ' $ / ' . $btc1 . ' <i class="fa fa-btc"></i></h6>';
						if (empty($vend_pic)) {
							echo '<h6 class="text-center"> Vendor : <img style="border-radius:200px;" src="../members/images/default.png" height="25px" width="25px"/> <a href="../members/@' . $vend_user . '-' . $vend_id . '-1.html">' . $vend_user . '</a></p>';
						} else {
							echo '<h6 class="card-text text-center">Vendor : <img style="border-radius:200px;" src="../members/uploads/' . $vend_pic . '" height="25px" width="25px" /> <a href="../members/@' . $vend_user . '-' . $vend_id . '-1.html">' . $vend_user . '</a></p>';
						}
						echo '</div>';

						echo '</div>';
						echo '</div>';
					}
				}
				?>


			</div>
			<!-- /.row -->
			<!-- PAGINATION HERE for the script here pagination_category -->
			<p id="pagination_controls"><?php echo $paginationCtrls; ?></p>
		</div>
		<!-- /.col-lg-9 -->
	</div>
	<!-- /.row -->
</div>
<!-- /.container -->


<!-- Bootstrap core JavaScript -->
<script src="../includes/vendor/jquery/jquery.min.js"></script>
<script src="../includes/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>