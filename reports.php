<?php include('header.php');

if(!isset($_SESSION['id'])) {
    header('Location: ../members/login.php');
}

$user = htmlspecialchars(@$_GET['user']);

$user_sca = $bdd->prepare("SELECT * FROM members WHERE username = ?");
$user_sca->execute(array($user));
$user_scam = $user_sca->fetch();

$user_scammer =  $user_scam['id'];
$usr_scammer =  $user_scam['username'];


// System for to add Report //

if(isset($_REQUEST['addRepBtn'])) {

        $report_txt = htmlspecialchars($_POST['report_txt']);
        $reason = htmlspecialchars($_POST['reason']);

    $insert_rep = $bdd->prepare("INSERT INTO reports (user_from,user_to,report_txt,reason,date_added) VALUES(?,?,?,?,NOW())");
    $insert_rep->execute(array($user_id,$usr_scammer,$report_txt,$reason));

    $success = "<div class='alert alert-success'>Report send successfully !</div>";
}

?>

<!-- Report HTML -->
<div class="container">

    <div class="col-lg-10 ">
        <h3 class="text-center">Report Scammer</h3>
    <form method="POST">
                  <?php echo @$errors; ?>
                  <?php echo @$success; ?>
                <label for="inputUsername">Username</label>
                <input style="background-color:#f1f1f1;cursor:not-allowed;" type="text" name="username" class="form-control-members" value="<?=$user?>" disabled>
                <label for="inputUsername">Reason</label>
                    <select name="reason" class="form-control">
                        <option>Scam</option>
                        <option>Spam</option>
                        <option>Fake Product</option>
                    </select>
                <label for="inputUsername">Description</label>
                <textarea class="form-control-members" id="editor1" name="report_txt" ></textarea>
                <script>
                  CKEDITOR.replace( 'editor1' );
              </script>
            <input style="margin-top: 20px;" type="submit" name="addRepBtn" class="btn btn-danger btn-block" value="Report <?=$user?>" >
          </form>
    </div>

</div>
